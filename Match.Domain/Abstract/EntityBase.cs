﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Match.Domain.Abstract
{
    public abstract class EntityBase<TId>
    {

        public TId Id { get; set; }
    }
}
