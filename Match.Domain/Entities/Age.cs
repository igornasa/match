﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;


namespace Match.Domain.Entities 
{
    public class Age : EntityBase<int>, IAttribute, IRangeFilter
    {
        public string Name { get; set; }
        public int RangeId { get; set; }
    }
}

