﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace Match.Domain.Entities
{
    public class Disability : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        [InverseProperty("DisabilityId")]
        public virtual IEnumerable<Profile> Profiles { get; set; }

       // [InverseProperty("SearchDisabilities")]
       // public virtual ICollection<Profile> SearchProfiles { get; set; }

        public static string[] InitValue = { "No Disability", "Hearing", "Mobility", "Vision" };
                                  
                                   
                                   


    

      }
}

