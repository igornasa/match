﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;

namespace Match.Domain.Entities
{
    public class EducationGrade : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }

        [InverseProperty("EducationGradeId")]
        public virtual IEnumerable<Profile> Profiles { get; set; }
       
       // [InverseProperty("SearchEducationGrades")]
       // public virtual ICollection<Profile> SearchProfiles { get; set; }

        

        public static string[] InitValue ={ "Elementary",
"High School",
"Associate degree",
"Bachelor's degree",
"Master's degree",
"Doctoral degree"};
      
    }
}
