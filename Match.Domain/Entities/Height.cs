﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    public class Height : EntityBase<int>, IAttribute ,IRangeFilter
    {
        public string Name { get; set; }
        public int RangeId {get; set;}
    }
}
