﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Match.Domain.Entities
{
    public class NumeralogyNumber
    {
        [Column(Order = 0), Key]
        public int Number1 { get; set; }
        [Column(Order = 1), Key]
        public int Number2 { get; set; }
        public string Info { get; set; }
    }

}
