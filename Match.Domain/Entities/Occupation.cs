﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    public class Occupation : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
      //  public virtual ICollection<Profile> Profiles { get; set; }

        public static string[] InitValue ={
                                            "Advertising",                                                 
                                            "Automotive",
                                            "Education",
                                            "IT",
                                            "Fashion",
                                            "Finance",
                                            "Food/Beverages",
                                            "Healthcare",
                                            "Pharmaceuticals",
                                            "Insurance",
                                            "Marketing",
                                            "Music",
                                            "Publishing",
                                            "Real Estate",
                                            "Telecommunications",
                                            "Travel/Tourism"};
    }
}
