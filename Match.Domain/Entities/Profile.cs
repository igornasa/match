﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;



namespace Match.Domain.Entities
{
    public enum Gender { Women = 1, Men = 2 };
    public enum YesNoAnswer { Yes = 1, No = 2 };
   


    public class Profile  : EntityBase<int>, IAggregateRoot
    {

      
        public virtual ICollection<ProfileImage> ProfileImages { get; set; }

      
        public int  MainProfileImageId { get; set; }
     
        //---------------------------------
        [DisplayName("First Name")]
        [Required(ErrorMessage = "First Name can't be empty")]

        public string FirstName { get; set; }

        //---------------------------------------
        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Last Name can't be empty")]
        public string LastName { get; set; }
        //---------------------------------------

        
        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("City")]
        public string SearchCity { get; set; }

        [DisplayName("Zip Code")]
        public string ZipCode { get; set; }

        [DisplayName("Zip Code")]
        public string SearchZipCode { get; set; }


       /* [DisplayName("Photo")]
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }*/

        //--------------------------------
        [Required(ErrorMessage = "Email Address can't be empty")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string EMail { get; set; }
        //---------------------------------------

        [Required(ErrorMessage = "Phone Number can't be empty")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        //---------------------------------------

        [DisplayName("Few words about myself")]
        public string AboutMyself { get; set; }
        //---------------------------------------





        //http://geekswithblogs.net/80n/archive/2012/04/27/apply-a-datetime-format-in-an-asp.net-mvc-textboxfor.aspx       
      //  [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]

        [DisplayFormat(DataFormatString = "{0:d}")]
       // [DataType(DataType.Date)]
        [DisplayName("Date of Birth")]
        [Required(ErrorMessage = "Date of Birth can't be empty")]
        public DateTime DateBirth { get; set; }





        public int AgeFromDateBirth
        {
             get { 
                
                 DateTime today = DateTime.Today;
                 int age = today.Year - DateBirth.Year;
                 if (DateBirth > today.AddYears(-age)) age--;
                
                 return age; 
               }
           
        }

      

        // Simple Filter
        //----------------------------------------
        [DisplayName("Gender")]
        [Range(1, int.MaxValue, ErrorMessage = "Gendor must be selected")]
        public int GenderId { get; set; }

        [NotMapped]
        public Gender GenderValue
        {
            get { return (Gender)GenderId; }
            set { GenderId = (int)value; }
        }



        [DisplayName("Gender")]
        [Required]
        public int LookingForId { get; set; }
        [NotMapped]
        public Gender LookingForValue
        {
            get { return (Gender)LookingForId; }
            set { LookingForId = (int)value; }
        }



        //-------- Numerology Filter
        //-----------------------
        public int PersonalityNumberId { get; set; }
        public PersonalityNumber PersonalityNumber
        {
            get
            {
                return PersonalityNumber.GetPersonalityNumber(PersonalityNumberId);
            }

        }
        

        //-------- Zodiac Filter
        //-----------------------
        public int ZodiacSignId { get; set; }

        public string ZodiacSignName
        {
            get
            {
                return ZodiacSign.GetZodiacSignName(ZodiacSignId);
            }

        }

        public ZodiacSign ZodiacSign
        {
            get
            {
                return ZodiacSign.GetZodiacSign(ZodiacSignId);
            }

        }

        //--------  Filter  OR    ----------------
        //--------------------------------------

        [Range(1, int.MaxValue, ErrorMessage = "Status must be selected")]
        [DisplayName("Status")]
        public int StatusId { get; set; }
        public Status Status { get; set; }

        [DisplayName("Status")]
        //public virtual ICollection<Status> SearchStatuses { get; set; }
        public long SearchStatusIdMask { get; set; }
       // public long StatusIdMask { get; set; }

        [NotMapped]
        public int[] SearchStatusIds { get; set; }

        

        
        //---------------------------------------
        [DisplayName("Education")]
        [Range(1, int.MaxValue, ErrorMessage = "Education must be selected")]
        public int EducationGradeId { get; set; }
        public EducationGrade EducationGrade { get; set; }

        [DisplayName("Education")]
        public long SearchEducationGradeIdMask { get; set; }
       // public long EducationGradeIdMask { get; set; }
        [NotMapped]
        public int[] SearchEducationGradeIds { get; set; }


        //-----------------------------------------
        [DisplayName("Religion")]
        [Range(1, int.MaxValue, ErrorMessage = "Religion must be selected")]
        public int ReligionId { get; set; }
        public Religion Religion { get; set; }

        [DisplayName("Religion")]
        public long SearchReligionIdMask { get; set; }
       // public long ReligionIdMask { get; set; }

        [NotMapped]
        public int[] SearchReligionIds { get; set; }

        //--------------------------------------------------------------

     
        [DisplayName("Smoke")]
        [Range(1, int.MaxValue, ErrorMessage = "Smoke habit must be selected")]
        public int SmokeId { get; set; }
        public Smoke Smoke { get; set; }

        [DisplayName("Smoke")]
        public long SearchSmokeIdMask { get; set; }
       // public long SmokeIdMask { get; set; }
        [NotMapped]
        public int[] SearchSmokeIds { get; set; }


        //-------------------------------------
        [DisplayName("Disability")]
        [Range(1, int.MaxValue, ErrorMessage = "Disability must be selected")]
        public int DisabilityId { get; set; }
        public Disability Disability { get; set; }

        [DisplayName("Disability")]
        public long SearchDisabilityIdMask { get; set; }
       // public long DisabilityIdMask { get; set; }

         [NotMapped]
        public int[] SearchDisabilityIds { get; set; }

        //------------------------------------------
        [DisplayName("Race")]
        [Range(1, int.MaxValue, ErrorMessage = "Race must be selected")]
        public int RaceId { get; set; }
        public Race Race { get; set; }

        [DisplayName("Race")]
        public long SearchRaceIdMask { get; set; }
       // public long RaceIdMask { get; set; }

        [NotMapped]
        public int[] SearchRaceIds { get; set; }

        //------------------- 

        [DisplayName("Occupation")]
        [Range(1, int.MaxValue, ErrorMessage = "Occupation must be selected")]
        public int OccupationId { get; set; }
        public Occupation Occupation { get; set; }

        [DisplayName("Occupation")]
        public long SearchOccupationIdMask { get; set; }
       // public long OccupationIdMask { get; set; }

        [NotMapped]
        public int[] SearchOccupationIds { get; set; }





       //-------------- Filter From-To-----------
        //------------------------------------------
        [DisplayName("Income")]
        [Range(1, int.MaxValue, ErrorMessage = "Income must be selected")]
        public int IncomeId { get; set; }
        public Income Income { get; set; }

        [DisplayName("Income")]
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int SearchIncomeIdFrom { get; set; }
         [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int SearchIncomeIdTo { get; set; }
        [ForeignKey("SearchIncomeIdFrom")]
        public Income SearchIncomeFrom { get; set; }
        [ForeignKey("SearchIncomeIdTo")]
        public Income SearchIncomeTo { get; set; }


        //------------------------------

       [DisplayName("Height")]
       [Range(1, int.MaxValue, ErrorMessage = "Height must be selected")]
       public int HeightId { get; set; }
       public Height Height { get; set; }

       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchHeightIdFrom { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchHeightIdTo { get; set; }
       [ForeignKey("SearchHeightIdFrom")]
       public Height SearchHeightFrom { get; set; }
        [ForeignKey("SearchHeightIdTo")]
       public Height SearchHeightTo { get; set; }
       


        //--------------------------------------
       [DisplayName("Weight")]
       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int WeightId { get; set; }
       public Weight Weight { get; set; }

       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchWeightIdTo { get; set; }
       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchWeightIdFrom { get; set; }
       [ForeignKey("SearchWeightIdFrom")]
       public Weight SearchWeightFrom { get; set; }
       [ForeignKey("SearchWeightIdTo")]
       public Weight SearchWeightTo { get; set; }



    //-------------------------------------
       [DisplayName("Age")]
       public int AgeId { get; set; }

       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchAgeIdFrom { get; set; }
       [ForeignKey("SearchAgeIdTo")]
       public Age SearchAgeTo { get; set; }

       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchAgeIdTo { get; set; }
       [ForeignKey("SearchAgeIdFrom")]
       public Age SearchAgeFrom { get; set; }
       

       

      
       //---------------------------------
       [DisplayName("Children")]
       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int ChildrenId { get; set; }
       public Children Children { get; set; }

       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchChildrenIdFrom { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
       public int SearchChildrenIdTo { get; set; }

        [ForeignKey("SearchChildrenIdFrom")]
       public Children SearchChildrenFrom { get; set; }
       [ForeignKey("SearchChildrenIdTo")]
       public Children SearchChildrenTo { get; set; }







        // Other
       //-----------------------------------------------
      

       [DisplayName("Country")]
       [Range(1, int.MaxValue, ErrorMessage = "Religion must be selected")]
       public int CountryId { get; set; }
       public Country Country { get; set; }
       [DisplayName("Country")]
       public int SearchCountryId { get; set; }

       
       [DisplayName("State")]
       [Range(1, int.MaxValue, ErrorMessage = "State must be selected")]
       public int StateId { get; set; }
       public State State { get; set; }
       [DisplayName("State")]
       public int SearchStateId { get; set; }

      
        
        

        //----------------------------------------------------------
     /*   public void SaveImageFromDisc(string filePath)
        {
            //open file from the disk (file path is the path to the file to be opened)
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                ImageData = new byte[fileStream.Length];
                fileStream.Read(ImageData, 0, (int)fileStream.Length);
                ImageMimeType = "image/jpeg";
            }
        }*/
        //-------------------------------------------------------


       
       
    }
}
