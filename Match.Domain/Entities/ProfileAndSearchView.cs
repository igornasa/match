﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;



namespace Match.Domain.Entities
{
   



    public class ProfileAndSearchView : EntityBase<int>, IAggregateRoot
    {


        public Profile Profile { get; set; }


        [DisplayName("Status")]
        public  ICollection<Status> SearchStatuses { get; set; }

        public int isOpen { get; set; }

        //---------------------------------------
      

        [DisplayName("Education")]
        public ICollection<EducationGrade> SearchEducationGrades { get; set; }
       
       

        //-----------------------------------------
        

        [DisplayName("Religion")]
        public  ICollection<Religion> SearchReligions { get; set; }
       
       
        //--------------------------------------------------------------


        [DisplayName("Smoke")]
        public ICollection<Smoke> SearchSmokes { get; set; }
        
       
        //-------------------------------------
       

        [DisplayName("Disability")]
        public ICollection<Disability> SearchDisabilities { get; set; }

       
        
        //------------------------------------------
       

        [DisplayName("Race")]
         public ICollection<Race> SearchRaces { get; set; }

        
       
        //------------------- 

        [DisplayName("Occupation")]
        public ICollection<Occupation> SearchOccupations { get; set; }
        
    }
}
