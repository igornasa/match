﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace Match.Domain.Entities
{
    public class Race : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        [InverseProperty("RaceId")]
        public virtual IEnumerable<Profile> Profiles { get; set; }

        //[InverseProperty("SearchRaces")]
        //public virtual ICollection<Profile> SearchProfiles { get; set; }

        public static string[] InitValue = { "White or Caucasian",
                    "African American",
                    "Hispanic",
                    "Asian",
"Native Hawaiian",
"American Indian", 
"Alaska Native",
"Other race" };
    }
}
