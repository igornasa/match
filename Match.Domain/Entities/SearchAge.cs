﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    public  class SearchAge : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        //public virtual ICollection<Profile> Profiles { get; set; }

        //public static string[] InitValue = { "Under 18", "18 - 24", "25 - 34", "35 - 44", "45 - 54", "55 - 64", "65 and over" };
    }
}
