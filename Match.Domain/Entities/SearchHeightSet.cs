﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities 
{
    public class SearchHeightSet :EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        IEnumerable<Profile> Profiles { get; set; }
    }
}
