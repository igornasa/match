﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    public class SearchWeight : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        //public virtual ICollection<Profile> Profiles { get; set; }
    }
}
