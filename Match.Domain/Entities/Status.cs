﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace Match.Domain.Entities
{
    public class Status : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        [InverseProperty("StatusId")]
        public virtual IEnumerable<Profile> Profiles { get; set; }

       // [InverseProperty("SearchStatuses")]
       // public virtual ICollection<Profile> SearchProfiles { get; set; }
       
       public static string[] InitValue ={ "Single","Separated","Divorced","Widowed", "Married"};
                                     
    
    }
}
