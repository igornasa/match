﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    //------------------------------------------
    public class ZodiacCompatibilityLevel : EntityBase<int>, IAttribute
    {
        public  string Name { get; set; }
        public string Description { get; set; }
    }
    //--------------------------------------
    public class ZodiacSign : EntityBase<int>, IAttribute
    {

        public static ZodiacCompatibilityLevel[] ZodiacCompatibilityLevels = { 
            new ZodiacCompatibilityLevel() { Id = 1, Name = "High compatibility",
            Description = "Two people whose zodiac signs are highly compatible will get along quite easily no matter how careless, hasty and upset they might be at times. They are just \"on the same wavelength\" most of the time."},
            
            new ZodiacCompatibilityLevel() { Id = 2, Name = "Good compatibility",
             Description = "Two people whose zodiac signs are less compatible will need to constantly \"tune in\" to each other's \"wavelength\". They have to be careful, patient and tactful if they really want to achieve harmony in their relationship. This requires a (quasi) permanent effort. It's not easy, but it can be done. Good understanding between two highly compatible zodiac signs comes (almost) naturally. Anyone can do it."},
           
             new ZodiacCompatibilityLevel() { Id = 3, Name = "Fairly compatible",  Description ="Degree of compatibility between two zodiac signs simply means how easily they can get along. In other words, a good understanding can always be achieved, but sometimes this comes easily and sometimes this takes patience and a lot of effort." },

            new ZodiacCompatibilityLevel() { Id = 4, Name = "Low compatibility" ,  Description ="Degree of compatibility between two zodiac signs simply means how easily they can get along. In other words, a good understanding can always be achieved, but sometimes this comes easily and sometimes this takes patience and a lot of effort."}};

    
        
       
        public string Name { get; set; }

        [DisplayName("High compatibility")]
        public int HighMask { get; set; }

        [DisplayName("Good compatibility")]
        public int GoodMask { get; set; }

        [DisplayName("Fairly compatible")]
        public int FairlyMask { get; set; }

        [DisplayName("Low compatibility")]
        public int LowMask { get; set; }

        //----------------------------------------------------------



        public  static ZodiacCompatibilityLevel GetCompatibility(int p1, int p2)
        {
            ZodiacSign z1 = GetZodiacSign(p1);
            ZodiacSign z2 = GetZodiacSign(p2);
            int id =0;
            int maskZ1 = (int)(Math.Pow(2, z1.Id));
            if ((z2.HighMask & maskZ1) > 0)  id=1;
            if ((z2.GoodMask & maskZ1) > 0) id=2;
            if ((z2.FairlyMask & maskZ1) > 0) id= 3;
            if ((z2.LowMask & maskZ1) > 0) id= 4;
            ZodiacCompatibilityLevel c = id == 0 ? null : ZodiacCompatibilityLevels[id - 1];
            return c;
            
        }

        //----------------------------------------------
        public static string GetZodiacSignName(int Id)
        { 
        
            if (  Id== 1)  return   "Aires";

            if (  Id== 2)  return   "Taurus";

            if (  Id== 3)  return   "Gemini";
                
            if (  Id== 4)  return   "Cancer";
                
            if (  Id== 5)  return   "leo";
                
            if (  Id== 6)  return   "Virgo";

            if (  Id== 7)  return   "Libra";

             if (  Id== 8)  return   "Scorpio";

            if (  Id== 9)  return   "Sagittarius";

            if (  Id== 10)  return   "Capricorn";
  
            if (  Id== 11)  return   "Aquarius";
                
            if (  Id== 12)  return   "Pisces";

            return "";
        
        }

        
        //-------------------------------------------------------------------------
            public static int GetZodiacSignId(DateTime DateBirth)
         {

              int month = Int32.Parse((DateBirth.Month).ToString());
              int day = Int32.Parse((DateBirth.Day).ToString());

              if ((month == 3 && day >= 21 && day <= 31) || (month == 4 && day >= 01 && day <= 20))
              {
                  return  1;
              }
              if ((month == 4 && day >= 21 && day <= 31) || (month == 5 && day >= 01 && day <= 21))
              {
                 return 2;
              }
              if ((month == 5 && day >= 22 && day <= 31) || (month == 6 && day >= 01 && day <= 21))
              {
                  return 3;
              }
              

          if ((month == 6 && day >= 22 && day <= 31) || (month == 7 && day >= 01 && day <= 22))
                {
                   return 4;
                }
                if ((month == 7 && day >= 23 && day <= 31) || (month == 8 && day >= 01 && day <= 22))
                {
                   return 5;
                }
                if ((month == 8 && day >= 23 && day <= 31) || (month == 9 && day >= 01 && day <= 23))
                {
                   return 6;
                }

                if ((month == 9 && day >= 24 && day <= 31) || (month == 10 && day >= 01 && day <= 23))
                {
                   return 7;
                }


                if ((month == 10 && day >= 24 && day <= 31) || (month == 11 && day >= 01 && day <= 22))
                {
                   return 8;
                }


                if ((month == 11 && day >= 23 && day <= 31) || (month == 12 && day >= 01 && day <= 21))
                {
                   return 9;
                }

                if ((month == 12 && day >= 22 && day <= 31) || (month == 1 && day >= 01 && day <= 20))
                {
                   return 10;
                }

                if ((month == 1 && day >= 21 && day <= 31) || (month == 2 && day >= 01 && day <= 19))
                {
                   return 11;
                }
                if ((month == 2 && day >= 20 && day <= 31) || (month == 3 && day >= 01 && day <= 20))
                {
                   return 12;
                }

                return 0;
            }
       


        //---------------------------------------------------
        public static  ZodiacSign GetZodiacSign(DateTime DateBirth)
        {
            int id = GetZodiacSignId(DateBirth);
            return GetZodiacSign(id);
        }


        //--------------------------------------------------------------------------
        public static  ZodiacSign GetZodiacSign(int Id)
         {
                 if (Id ==1)
                {   return new ZodiacSign() {
                    Id = 1,
                    Name="Aires",
                    HighMask = (int)(Math.Pow(2, 3) +Math.Pow(2, 5) +Math.Pow(2, 9)  + Math.Pow(2, 11)),
                    GoodMask = (int)(Math.Pow(2, 1)  + Math.Pow(2, 7)),
                    FairlyMask = (int)(Math.Pow(2, 2) +Math.Pow(2, 6) +Math.Pow(2, 8)  + Math.Pow(2, 12)),
                    LowMask = (int)(Math.Pow(2, 4)  + Math.Pow(2, 10))}; 
                };
                //---------------------------------------------------------------------------------------
               
                if (Id ==2)
                {
                    return new ZodiacSign() {
                    Id = 2,
                    Name="Taurus",
                    HighMask = (int)(Math.Pow(2, 4) +Math.Pow(2, 6) +Math.Pow(2, 10) +Math.Pow(2, 12)),
                    GoodMask =  (int)(Math.Pow(2, 2) +Math.Pow(2, 8)),
                    FairlyMask = (int)(Math.Pow(2, 1) +Math.Pow(2, 3) +Math.Pow(2, 7) +Math.Pow(2, 9) ),
                    LowMask = (int) (Math.Pow(2, 5) +Math.Pow(2, 11))};  
                };

                 //------------------------------------------------------------------------------------------
                //3
                 if (Id ==3)
                {
                    return new ZodiacSign() {
                    Id = 3,
                     Name="Gemini",
                    HighMask =  (int)(Math.Pow(2, 1) + Math.Pow(2, 5) + Math.Pow(2, 7) + Math.Pow(2, 11)),
                    GoodMask = (int)(Math.Pow(2, 3) + Math.Pow(2, 9)),
                    FairlyMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 4) + Math.Pow(2, 8) + Math.Pow(2, 10)),
                    LowMask =(int)(Math.Pow(2, 6) + Math.Pow(2, 12))}; 
                 };

                //4 Zodiac Sign Compatibility
                     //----------------------------
                if (Id ==4)
                 {    return new ZodiacSign() {
                    Id = 4,
                    Name ="Cancer",
                    HighMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 12)),
                    GoodMask =(int)(Math.Pow(2, 4) + Math.Pow(2, 10)),
                    FairlyMask =(int)(Math.Pow(2, 3) + Math.Pow(2, 5) + Math.Pow(2, 9) + Math.Pow(2, 11)),
                    LowMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 7))}; 
                };

   
                //5 Zodiac Sign Compatibility
                    //--------------------------------
                if (Id ==5)
                 {    return new ZodiacSign() {
                    Id = 5,
                    Name = "Leo",
                    HighMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 3) + Math.Pow(2, 7) + Math.Pow(2, 9)),
                    GoodMask =(int)(Math.Pow(2, 5) + Math.Pow(2, 11)),
                    FairlyMask =(int)(Math.Pow(2, 4) + Math.Pow(2, 6) + Math.Pow(2, 10) + Math.Pow(2, 12)),
                    LowMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 8))}; 
                };

                //6 Zodiac Sign Compatibility
                    //----------------------------------
                if (Id ==6)
                {    return new ZodiacSign() {
                    Id = 6,
                    Name = "Virgo",
                    HighMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 4) + Math.Pow(2, 8) + Math.Pow(2, 10)),
                    GoodMask =(int)( Math.Pow(2, 6) + Math.Pow(2, 12)),
                    FairlyMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 5) + Math.Pow(2, 7) + Math.Pow(2, 11)),
                    LowMask =(int)(Math.Pow(2, 3) + Math.Pow(2, 9))}; 
                };

                //7 Zodiac Sign Compatibility
                    //---------------------------
                if (Id ==7)
                {    return new ZodiacSign() {
                    Id = 7,
                    Name = "Libra",
                    HighMask =(int)(Math.Pow(2, 3) + Math.Pow(2, 5) + Math.Pow(2, 9) + Math.Pow(2, 11)),
                    GoodMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 7)),
                    FairlyMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 12)),
                    LowMask =(int)(Math.Pow(2, 4) + Math.Pow(2, 10))}; 
                };

                //8 Zodiac Sign Compatibility
                //-------------------------------------
                if (Id ==8)
                {    return new ZodiacSign() {
                    Id = 8,
                    Name = "Scorpio",
                    HighMask =(int)(Math.Pow(2, 4) + Math.Pow(2, 6) + Math.Pow(2, 10) + Math.Pow(2, 12)),
                    GoodMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 8)),
                    FairlyMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 3) + Math.Pow(2, 7) + Math.Pow(2, 9)),
                    LowMask =(int)(Math.Pow(2, 5) + Math.Pow(2, 11))}; 
                };

                //9 Zodiac Sign Compatibility
                //----------------------------------
                if (Id ==9)
                {    return new ZodiacSign() {
                    Id = 9,
                    Name = "Sagittarius",
                    HighMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 5) + Math.Pow(2, 7) + Math.Pow(2, 11)),
                    GoodMask =(int)(Math.Pow(2, 3) + Math.Pow(2, 9)),
                    FairlyMask = (int)(Math.Pow(2, 2) + Math.Pow(2, 4) + Math.Pow(2, 8) + Math.Pow(2, 10)),
                    LowMask =(int)(Math.Pow(2, 6) + Math.Pow(2, 12))}; 
                };

                //10 Zodiac Sign Compatibility
                //------------------------------------
                 if (Id ==10)
                {    return new ZodiacSign() {
                    Id = 10,
                    Name = "Capricorn",
                    HighMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 12)),
                    GoodMask =(int)(Math.Pow(2, 4) + Math.Pow(2, 10)),
                    FairlyMask =(int)(Math.Pow(2, 3) + Math.Pow(2, 5) + Math.Pow(2, 9) + Math.Pow(2, 11)),
                    LowMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 7))}; 
                };

                //11 Zodiac Sign Compatibility
                //--------------------------------
                if (Id ==11)
                {    return new ZodiacSign() {
                    Id = 11,
                    Name = "Aquarius",
                    HighMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 3) + Math.Pow(2, 7) + Math.Pow(2, 9)),
                    GoodMask =(int)(Math.Pow(2, 5) + Math.Pow(2, 11)),
                    FairlyMask =(int)(Math.Pow(2, 4) + Math.Pow(2, 6) + Math.Pow(2, 10) + Math.Pow(2, 12)),
                    LowMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 8))}; 
                };

                //12 Zodiac Sign Compatibility
                //---------------------------------
                if (Id ==12)
                {    return new ZodiacSign() {
                    Id = 12,
                    Name = "Pisces",
                    HighMask =(int)(Math.Pow(2, 2) + Math.Pow(2, 4) + Math.Pow(2, 8) + Math.Pow(2, 10)),
                    GoodMask =(int)( Math.Pow(2, 6) + Math.Pow(2, 12)),
                    FairlyMask =(int)(Math.Pow(2, 1) + Math.Pow(2, 5) + Math.Pow(2, 7) + Math.Pow(2, 11)),
                    LowMask =(int)(Math.Pow(2, 3) + Math.Pow(2, 9))}; 
                };

              return new ZodiacSign() {
                    Id = 0, Name = "", HighMask = 0, GoodMask = 0,  FairlyMask = 0,  LowMask = 0

                };

         }
    }
}

                 //-------------------------------------------------------------------------------

/*

              

    { Id = 1, Sign ="Aires"}
   

{ Id = 2, Sign ="Taurus";
             
             
{ Id = 3, Sign ="Gemini";
                
{ Id = 4, Sign ="Cancer";
                
{ Id = 5, Sign ="leo";
                
{ Id = 6, Sign ="Virgo";
              

           
{ Id = 7, Sign ="Libra";

 { Id = 8, Sign ="Scorpio";
               

{ Id = 9, Sign ="Sagittarius";
               

{ Id = 10, Sign ="Capricorn";
                

               
{ Id = 11, Sign ="Aquarius";
                
{ Id = 12, Sign ="Pisces";
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 *
 * High astrological affinity:
    1‑3, 1‑5, 1‑9, 1‑11
Good compatibility:
    1‑1, 1‑7
Fairly compatible:
    1‑2, 1‑6, 1‑8, 1‑12
Low compatibility:
    1‑4, 1‑10

2 Zodiac Sign Compatibility

High astrological affinity:
    2‑4, 2‑6, 2‑10, 2‑12
Good compatibility:
    2‑2, 2‑8
Fairly compatible:
    2‑1, 2‑3, 2‑7, 2‑9
Low compatibility:
    2‑5, 2‑11

3 Zodiac Sign Compatibility

High astrological affinity:
    3‑1, 3‑5, 3‑7, 3‑11
Good compatibility:
    3‑3, 3‑9
Fairly compatible:
    3‑2, 3‑4, 3‑8, 3‑10
Low compatibility:
    3‑6, 3‑12

4 Zodiac Sign Compatibility

High astrological affinity:
    4‑2, 4‑6, 4‑8, 4‑12
Good compatibility:
    4‑4, 4‑10
Fairly compatible:
    4‑3, 4‑5, 4‑9, 4‑11
Low compatibility:
    4‑1, 4‑7

5 Zodiac Sign Compatibility

High astrological affinity:
    5‑1, 5‑3, 5‑7, 5‑9
Good compatibility:
    5‑5, 5‑11
Fairly compatible:
    5‑4, 5‑6, 5‑10, 5‑12
Low compatibility:
    5‑2, 5‑8

6 Zodiac Sign Compatibility

High astrological affinity:
    6‑2, 6‑4, 6‑8, 6‑10
Good compatibility:
    6‑6, 6‑12
Fairly compatible:
    6‑1, 6‑5, 6‑7, 6‑11
Low compatibility:
    6‑3, 6‑9

7 Zodiac Sign Compatibility

High astrological affinity:
    7‑3, 7‑5, 7‑9, 7‑11
Good compatibility:
    7‑1, 7‑7
Fairly compatible:
    7‑2, 7‑6, 7‑8, 7‑12
Low compatibility:
    7‑4, 7‑10

8 Zodiac Sign Compatibility

High astrological affinity:
    8‑4, 8‑6, 8‑10, 8‑12
Good compatibility:
    8‑2, 8‑8
Fairly compatible:
    8‑1, 8‑3, 8‑7, 8‑9
Low compatibility:
    8‑5, 8‑11

9 Zodiac Sign Compatibility

High astrological affinity:
    9‑1, 9‑5, 9‑7, 9‑11
Good compatibility:
    9‑3, 9‑9
Fairly compatible:
    9‑2, 9‑4, 9‑8, 9‑10
Low compatibility:
    9‑6, 9‑12

10 Zodiac Sign Compatibility

High astrological affinity:
    10‑2, 10‑6, 10‑8, 10‑12
Good compatibility:
    10‑4, 10‑10
Fairly compatible:
    10‑3, 10‑5, 10‑9, 10‑11
Low compatibility:
    10‑1, 10‑7

11 Zodiac Sign Compatibility

High astrological affinity:
    11‑1, 11‑3, 11‑7, 11‑9
Good compatibility:
    11‑5, 11‑11
Fairly compatible:
    11‑4, 11‑6, 11‑10, 11‑12
Low compatibility:
    11‑2, 11‑8

12 Zodiac Sign Compatibility

High astrological affinity:
    12‑2, 12‑4, 12‑8, 12‑10
Good compatibility:
    12‑6, 12‑12
Fairly compatible:
    12‑1, 12‑5, 12‑7, 12‑11
Low compatibility:
    12‑3, 12‑9

﻿
Astrologer: Ariana




//------------------------------------------------------------------------------------------------------------------------------------
 if (((month == 3) && (day >= 21 || day <= 31)) || ((month == 4) && (day >= 01 || day <= 20)))
                {
                    zodiac = 1;
                }
                if (((month == 4) && (day >= 21 || day <= 31)) || ((month == 5) && (day >= 01 || day <= 21)))
                {
                    zodiac = 2;
                }
                if (((month == 5) && (day >= 21 || day <= 31)) || ((month == 6) && (day >= 01 || day <= 21)))
                {
                    zodiac = 3;
                }
                if (((month == 6) && (day >= 22 || day <= 31)) || ((month == 7) && (day >= 01 || day <= 22)))
                {
                    zodiac = 4;
                }
                if (((month == 7) && (day >= 23 || day <= 31)) || ((month == 8) && (day >= 01 || day <= 22)))
                {
                    zodiac = 5;
                }
                if (((month == 8) && (day >= 23 || day <= 31)) || ((month == 9) && (day >= 01 || day <= 21)))
                {
                    zodiac = 6;
                }

                if (((month == 9) && (day >= 22 || day <= 31)) || ((month == 10) && (day >= 01 || day <= 22)))
                {
                    zodiac = 7;
                }


                if (((month == 10) && (day >= 23 || day <= 31)) || ((month == 11) && (day >= 01 || day <= 21)))
                {
                    zodiac = 8;
                }


                if (((month == 11) && (day >= 22 || day <= 31)) || ((month == 12) && (day >= 01 || day <= 21)))
                {
                    zodiac = 9;
                }

                if (((month == 11) && (day >= 22 || day <= 31)) || ((month == 1) && (day >= 01 || day <= 20)))
                {
                    zodiac = 10;
                }

                if (((month == 1) && (day >= 22 || day <= 31)) || ((month == 2) && (day >= 01 || day <= 20)))
                {
                    zodiac = 11;
                }
                if (((month == 2) && (day >= 20 || day <= 31)) || ((month == 3) && (day >= 01 || day <= 20)))
                {
                    zodiac = 12;
                }
*/

  