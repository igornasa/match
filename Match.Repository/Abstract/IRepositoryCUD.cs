﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Repository.Abstract
{
    // CRUD -> C*UD
    public interface IRepositoryCUD<T, TId> where T : class, IAggregateRoot
    {
        void SaveProfileInfo(T entity, int mainImageOrderId);
        void Add(T entity);
        void Remove(TId id);
        
    }
}
