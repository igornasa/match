﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Match.Domain.Abstract;
using Match.Repository.EF;
using Match.Domain.Entities;



namespace Match.Repository.Abstract
{
    // CRUD -> *R**
    public interface IRepositoryR<T, TId> where T: class, IAggregateRoot  
    {


        IQueryable<T> GetDBSet();

        string GetEntitySetName();

        //ProfileForDisplay FindBy(int idForMenu, int idForDisplay);
        T FindBy(TId id, bool isFullInfo = false);
        IQueryable<T> FindAll();
        ResponseModel FindAllFor(int id, bool giveAllForNew);
        //IQueryable<T> FindBy(Query query, int index, int count);
        IQueryable<T> FindByQuery(T p);
        ResponseModel FindByQuery(int id, bool useNumerology,  bool useZodiac);
       //IEnumerable<ProfileForDisplay> FindAllForPage(IEnumerable<T> profiles,  int pageNumber, int itemsPerPage);
       ProfileImage FindProfileImageBy(int imageId);

        
         
        
    }
    
}
