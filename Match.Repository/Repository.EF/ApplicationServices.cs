﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Configuration;
using System.Web.Management;
using System.Web.Security;
using WebMatrix.WebData;


namespace Match.Repository.EF
{
    
  public static class ApplicationServices
  {
      readonly static string defaultConnectionString = WebConfigurationManager.AppSettings["DbContextEF"];
      readonly static string connectionString = WebConfigurationManager.ConnectionStrings[defaultConnectionString].ConnectionString;
      readonly static SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder(connectionString);

    public static void InstallServices(SqlFeatures sqlFeatures)
    {
      SqlServices.Install(myBuilder.InitialCatalog, sqlFeatures, connectionString);
    }

    public static void UninstallServices(SqlFeatures sqlFeatures)
    {
      SqlServices.Uninstall(myBuilder.InitialCatalog, sqlFeatures, connectionString);
    }
  }
}
