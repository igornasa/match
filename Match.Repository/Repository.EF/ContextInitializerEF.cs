﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using Match.Domain.Entities;
using Match.Domain.Abstract;
using System.IO;
using Match.Repository.ContextStorage;
using System.Web.Security;
using WebMatrix.WebData;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Web.Management;
using System.Web;
using System.Web.Routing;




namespace Match.Repository.EF
{
   // public class ContextInitializerEF :  DropCreateDatabaseIfModelChanges <DbContextEF>
  public class ContextInitializerEF :  DropCreateDatabaseAlways <DbContextEF>
    {
        protected override void Seed(DbContextEF context)
        {
            base.Seed(context);
          
            SeedDB(context);
            
        }

        
        
         
    //-----------------------------------------------------------
    private void SeedDB(DbContextEF db)
    {
       
        ProfileImage pImage;
        WebSecurity.InitializeDatabaseConnection("DbContextEF",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);
        
        // Roles
        if (!Roles.RoleExists("User"))
        {
            Roles.CreateRole("User");
        }
        if (!Roles.RoleExists("Admin"))
        {
            Roles.CreateRole("Admin");
        }

        // Account
        //---------------------------------------
        if (Membership.GetUser("anna", false) == null)
        {
            WebSecurity.CreateUserAndAccount("anna", "password1");
        }

        if (Membership.GetUser("borgia", false) == null)
        {
            WebSecurity.CreateUserAndAccount("borgia", "password2");
        }
        if (Membership.GetUser("user3", false) == null)
        {
            WebSecurity.CreateUserAndAccount("user3", "password3");
        }

        if (Membership.GetUser("lucr4", false) == null)
        {
            WebSecurity.CreateUserAndAccount("lucr4", "password4");
        }
        if (Membership.GetUser("richard3", false) == null)
        {
            WebSecurity.CreateUserAndAccount("richard3", "password5");
        }
        if (Membership.GetUser("henry8", false) == null)
        {
            WebSecurity.CreateUserAndAccount("henry8", "password6");
        }
        if (Membership.GetUser("user7", false) == null)
        {
            WebSecurity.CreateUserAndAccount("user7", "password7");
        }
        if (Membership.GetUser("user8", false) == null)
        {
            WebSecurity.CreateUserAndAccount("user8", "password8");
        }
        if (Membership.GetUser("user9", false) == null)
        {
            WebSecurity.CreateUserAndAccount("user9", "password9");
        }
         if (Membership.GetUser("user10", false) == null)
        {
            WebSecurity.CreateUserAndAccount("user10", "password10");
        }

         if (Membership.GetUser("user11", false) == null)
         {
             WebSecurity.CreateUserAndAccount("user11", "password11");
         }

         if (Membership.GetUser("user12", false) == null)
         {
             WebSecurity.CreateUserAndAccount("user12", "password12");
         }
       
       

        // Add Roles ([]usernames, [] roleNames)
        //--------------------------------------------------------
        if (!Roles.GetRolesForUser("anna").Contains("User"))
        {
            Roles.AddUsersToRoles(new[] { "anna", "borgia", "user3", "lucr4", "richard3", "henry8",
                "user7","user8","user9","user10","user11","user12" }, new[] { "User" });
        } 
       


        

       // ApplicationServices.InstallServices(SqlFeatures.Membership);
       
            

           
        //------------------------------------------------------------------
            SetInitDataForIncrement<Income>("${0}.000", 15, 200, 5, db);
            SetInitDataForIncrement<Income>("${0}.000", 200, 1000, 50, db);
           

            SetInitData<Status>(Status.InitValue, db);
            SetInitData<Occupation>(Occupation.InitValue, db);
            SetInitData<Race>(Race.InitValue, db);            
            SetInitData<Religion>(Religion.InitValue, db);
            SetInitData<Country>(Country.InitValue, db);
            SetInitDataForState(State.InitValue, db); // state!


            SetInitDataForRangeInt<Weight, SearchWeight>("{0} {1}", "{0} - {1} {2}", "lb",
                                                        100, 300, 5, 30, db);

            SetInitDataForRangeDouble<Height, SearchHeight>("{0:0.0}{1}", "{0:0.0} - {1:0.0}{2}", "\"",                                                  
                                                     40, 85, 1, 5, db);

           SetInitDataForRangeInt<Age, SearchAge>("{0}{1}", "{0} - {1} {2}","",
                                                       17, 99, 1, 5, db);


            SetInitData<Smoke>(Smoke.InitValue, db);
            SetInitData<Children>(Children.InitValue, db);
            SetInitData<Disability>(Disability.InitValue, db);
            SetInitData<EducationGrade>(EducationGrade.InitValue, db);

        //-----------------------------------
            db.SaveChanges();

            //C:\Documents and Settings\IRINA\My Documents\Visual Studio 2010\Projects\Match\
           string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/Images/", "");


            List<ProfileImage> pImages1 = new List<ProfileImage>();
            pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
            pImage.SaveImageFromDisc(path + "anneboleyn3.jpg");
            pImages1.Add(pImage);
            db.Set<ProfileImage>().Add(pImage);

            pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
            pImage.SaveImageFromDisc(path + "anneboleyn2.jpg");
            pImages1.Add(pImage);
            db.Set<ProfileImage>().Add(pImage);

            pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
            pImage.SaveImageFromDisc(path + "anneboleyn1.jpg");
            pImages1.Add(pImage);
            db.Set<ProfileImage>().Add(pImage);

           

            pImage = new ProfileImage() { Id = 4, ImageOrderId = 4 }; //54
            pImage.SaveImageFromDisc(path + "anneboleyn5.jpg");
            pImages1.Add(pImage);
            db.Set<ProfileImage>().Add(pImage);

            pImage = new ProfileImage() { Id = 5, ImageOrderId = 5 }; //5
            pImage.SaveImageFromDisc(path + "anneboleyn4.jpg");
            pImages1.Add(pImage);
            db.Set<ProfileImage>().Add(pImage);

            pImage = new ProfileImage() { Id = 6, ImageOrderId = 6 }; //6
            pImage.SaveImageFromDisc(path + "anneboleyn6.jpg");
            pImages1.Add(pImage);

            pImage = new ProfileImage() { Id = 7, ImageOrderId = 7 };//7
            pImage.SaveImageFromDisc(path + "genrih8.jpg");
            pImages1.Add(pImage);


            db.Set<ProfileImage>().Add(pImage);

          
            Profile userProfile1 = new Profile()
            {
                
               
                Id = 1,
                MainProfileImageId =1,
                FirstName = "Anne",
                LastName = "Boleyn",
                DateBirth = DateTime.Today,
                
                GenderValue = Gender.Women,
                LookingForValue = Gender.Men,
              
                EMail = "AnneBoleyn@yahoo.com",
                Phone = "(718) 646-6753",

                AboutMyself= "Anne Boleyn was Queen of England  as the second wife of King Henry VIII",
                
                 RaceId =1, 
                 OccupationId = 5,
                 IncomeId =6,

                 SearchStatusIdMask = (long)(Math.Pow(2, 1)+ Math.Pow(2, 2)),
                 SearchRaceIdMask = (long)(Math.Pow(2, 1)),
                 SearchOccupationIdMask =(long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                 SearchReligionIdMask = (long)(Math.Pow(2, 1)),
                 SearchDisabilityIdMask = (long)(Math.Pow(2, 1)),
                 SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                 SearchEducationGradeIdMask = (long)(Math.Pow(2, 5)),


                 

                 CountryId =195,
                 StateId =33,
                 City = "Brooklyn",
                 ZipCode = "11243",

                SearchIncomeIdFrom = 4,
                SearchIncomeIdTo = 50,

                HeightId = 15,
                SearchHeightIdFrom = 4,
                SearchHeightIdTo = 25,

                WeightId = 22,
                SearchWeightIdFrom = 5,
                SearchWeightIdTo = 30,

                AgeId=5,
                SearchAgeIdFrom = 4,
                SearchAgeIdTo = 24,

                ChildrenId = 2,
                SearchChildrenIdFrom = 1,
                SearchChildrenIdTo = 6,

                StatusId = 1,
                SmokeId =1,
                DisabilityId = 1,
                EducationGradeId = 2,
                ReligionId = 1,
                 
              
                SearchCountryId = 195, 
                SearchStateId = 33,
                SearchCity = "Bronx",
                SearchZipCode = "11243",
              
               
                ProfileImages = pImages1
            };
            userProfile1.DateBirth = new DateTime(1988, 1, 1);
            userProfile1.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile1.DateBirth);
            userProfile1.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile1.DateBirth);
           // userProfile1.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\Anna.jpg");
            db.Set<Profile>().Add(userProfile1);







            //=================================================================
                List<ProfileImage> pImages2 = new List<ProfileImage>();
                     pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                     pImage.SaveImageFromDisc(path + "borgia_lucrecia.jpg");
                     pImages2.Add(pImage);
                     db.Set<ProfileImage>().Add(pImage);

            

                     pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 }; //2
                     pImage.SaveImageFromDisc(path + "LucreziaBorgia2.jpg");
                     pImages2.Add(pImage);
                     db.Set<ProfileImage>().Add(pImage);


                     pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                     pImage.SaveImageFromDisc(path + "LucreziaBorgia1.jpg");
                     pImages2.Add(pImage);
                     db.Set<ProfileImage>().Add(pImage);

                     
                  
                     //------------------------------------------
                     Profile userProfile2 = new Profile()
                     {
                       
                      
                         Id = 2,
                         MainProfileImageId = 2,    
                         FirstName = "Lucrecia",
                         LastName = "Borgia",
                         DateBirth = DateTime.Today,
                         StatusId = 1,
                         GenderValue = Gender.Women,
                         LookingForValue = Gender.Men,
             
                         EMail = "LucreciaBorgia@yahoo.com",
                         Phone = "(212) 646-6753",

                         AboutMyself="Lucrezia Borgia was the daughter of future pope Alexander VI, and her three marriages into influential families helped build the political power of her own family. Historians debate whether or not Borgia was an active participant in her notorious family’s crimes",
                         SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                         SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                         SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                         SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                         + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                         SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                         SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                         SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),

                         RaceId = 1,
                         OccupationId = 1,
                         IncomeId = 6,
                      
                         //OccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                         SearchIncomeIdFrom = 4,
                         SearchIncomeIdTo = 8,


                         CountryId = 195,
                         StateId = 46,
                         City = "Washington",
                         ZipCode = "13243",


                         HeightId = 5,
                         SearchHeightIdFrom = 4,
                         SearchHeightIdTo = 45,

                         WeightId = 22,
                         SearchWeightIdFrom = 5,
                         SearchWeightIdTo = 40,

                         AgeId = 5,
                         SearchAgeIdFrom = 4,
                         SearchAgeIdTo = 24,

                         ChildrenId = 2,
                         SearchChildrenIdFrom = 1,
                         SearchChildrenIdTo = 4,

                         SmokeId = 1,
                         DisabilityId = 1,
                         EducationGradeId = 4,
                         ReligionId = 1,

               
                         SearchCountryId = 1,
                         SearchStateId = 1,
                         SearchCity = "NYC",
                         SearchZipCode = "11243",
                
                       
                
               
                         ProfileImages = pImages2
                     };

                     userProfile2.DateBirth =  new DateTime(1980, 11, 8);
                     userProfile2.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile2.DateBirth);
                     userProfile2.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile2.DateBirth);
                  //   userProfile2.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\borgia_lucrecia.jpg");
                     db.Set<Profile>().Add(userProfile2);





                     //================================================================

                               List<ProfileImage> pImages3 = new List<ProfileImage>();
                             pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                             pImage.SaveImageFromDisc(path + "153.jpg");
                             pImages3.Add(pImage);
                             db.Set<ProfileImage>().Add(pImage);

                             pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                             pImage.SaveImageFromDisc(path + "152.jpg");
                             pImages3.Add(pImage);
                             db.Set<ProfileImage>().Add(pImage);

                             pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                             pImage.SaveImageFromDisc(path + "151.jpg");
                             pImages3.Add(pImage);
                             db.Set<ProfileImage>().Add(pImage);

                             pImage = new ProfileImage() { Id = 4, ImageOrderId = 4 }; //3
                             pImage.SaveImageFromDisc(path + "154.jpg");
                             pImages3.Add(pImage);
                             db.Set<ProfileImage>().Add(pImage);


                          
                             //---------------------------------------------------------------------
                             Profile userProfile3 = new Profile()
                             {
                              
                              
                                 Id = 3,
                                 MainProfileImageId= 3,
                                 FirstName = "Philip",
                                 LastName = "Habsburg",
                                 DateBirth = DateTime.Today,
                                 StatusId = 1,
                                 GenderValue = Gender.Men,
                                 LookingForValue = Gender.Women,

                                 EMail = "Philip1Habsburg@yahoo.com",
                                 Phone = "(718) 665-6753",

                                 AboutMyself = "PhilipI known as Philip the Handsome or the Fair, was the first member of the house of Habsburg to be King of Castile. ",
                                 SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                 SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                 SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                 SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                 + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                 SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                 SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                 SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),

                                 RaceId = 1,
                                 OccupationId = 1,
                                 IncomeId = 6,
                              
                                 //OccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                 SearchIncomeIdFrom = 4,
                                 SearchIncomeIdTo = 8,


                                 CountryId = 195,
                                 StateId = 33,
                                 City = "Brooklyn",
                                 ZipCode = "11243",
                                 HeightId = 5,
                                 SearchHeightIdFrom = 4,
                                 SearchHeightIdTo = 45,

                                 WeightId = 22,
                                 SearchWeightIdFrom = 20,
                                 SearchWeightIdTo = 40,

                                 AgeId = 5,
                                 SearchAgeIdFrom = 4,
                                 SearchAgeIdTo = 24,

                                 ChildrenId = 2,
                                 SearchChildrenIdFrom = 1,
                                 SearchChildrenIdTo = 4,
                

                                 SmokeId = 1,
                                 DisabilityId = 1,
                                 EducationGradeId = 2,
                                 ReligionId = 1,

              
                                 SearchCountryId = 1,
                                 SearchStateId = 1,
                                 SearchCity = "Cologne",
                                 SearchZipCode = "11243",
                
                
               
                                 ProfileImages = pImages3
                             };
                             userProfile3.DateBirth = new DateTime(1979, 11, 8);
                             userProfile3.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile3.DateBirth);
                             userProfile3.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile3.DateBirth);
                           //  userProfile3.SaveImageFromDisc(path + "kon2.jpg");
                             db.Set<Profile>().Add(userProfile3);







                             //==============================================================


                                              List<ProfileImage> pImages4 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "Lucretia3.jpg");
                                              pImages4.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "Lucretia2.jpg");
                                              pImages4.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                                              pImage.SaveImageFromDisc(path + "Lucretia1.jpg");
                                              pImages4.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);



                                              List<Smoke> pSearchSmokes4 = new List<Smoke>();
                                              pSearchSmokes4.Add(db.Set<Smoke>().Find(4));
                                              pSearchSmokes4.Add(db.Set<Smoke>().Find(4));

                                              List<EducationGrade> pSearchEducationGrades4 = new List<EducationGrade>();
                                              pSearchEducationGrades4.Add(db.Set<EducationGrade>().Find(2));
                                              pSearchEducationGrades4.Add(db.Set<EducationGrade>().Find(4));

                                              List<Religion> pSearchReligion4 = new List<Religion>();
                                              pSearchReligion4.Add(db.Set<Religion>().Find(2));
                                              pSearchReligion4.Add(db.Set<Religion>().Find(4));


                                              List<Disability> pDisability4 = new List<Disability>();
                                              pDisability4.Add(db.Set<Disability>().Find(2));
                                              pDisability4.Add(db.Set<Disability>().Find(4));

                                              List<Status> pStatus4 = new List<Status>();
                                              pStatus4.Add(db.Set<Status>().Find(2));
                                              pStatus4.Add(db.Set<Status>().Find(4));


                                            
                                              Profile userProfile4 = new Profile()
                                              {
                                                
                                              
                                                  Id = 4,
                                                  MainProfileImageId = 4,
                                                  FirstName = "Lucretia",
                                                  LastName = "Collatinus",
                                                  DateBirth = DateTime.Today,
             
                                                  GenderValue = Gender.Women,
                                                  LookingForValue = Gender.Men,
             
                                                  EMail = "Collatinus@yahoo.com",
                                                  Phone = "(718) 436-6753",
                                                  AboutMyself = "Beautiful and virtuous wife of the nobleman Lucius Tarquinius Collatinus.",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),


                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                              
                                                  //OccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,


                                                  CountryId = 1,
                                                  StateId = 1,
                                                  City = "Brooklyn",
                                                  ZipCode = "11243",

                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,

                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,

                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,

                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,

                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,
                                                  StatusId = 2,

                                              
                                              
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
               
                
                
               
                                                  ProfileImages = pImages4
                                              };
                                              userProfile4.DateBirth = new DateTime(1970, 9, 8);
                                              userProfile4.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile4.DateBirth);
                                              userProfile4.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile4.DateBirth);
                                             // userProfile4.SaveImageFromDisc(path + "Lucretia3.jpg");
                                              db.Set<Profile>().Add(userProfile4);




                                              //========================================================================
                                              List<ProfileImage> pImages5 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "Richard.jpg");
                                              pImages5.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "Richard2.jpg");
                                              pImages5.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);



                                         
                                            
                                              Profile userProfile5 = new Profile()
                                              {
                                                
                                               
                                                  Id = 5,
                                                  MainProfileImageId = 5,
                                                  FirstName = "Richard",
                                                  LastName = "Gloster",
                                                  DateBirth = DateTime.Today,
                                                  StatusId = 1,
                                                  GenderValue = Gender.Men,
                                                  LookingForValue =  Gender.Women,
                                                  //PrenuptialAgreementId = 1,
                                                  EMail = "Gloster@google.com",
                                                  Phone = "(718) 646-6753",
                                                  AboutMyself = "King of England from 1483 until his death in 1485 in the Battle of Bosworth Field. He was the last king of the House of York and the last of the Plantagenet dynasty.",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),


                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                               
                                                  //OccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,


                                                  CountryId = 195,
                                                  StateId = 31,
                                                  City = "Elizabeth",
                                                  ZipCode = "41243",

                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,

                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,

                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,

                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,

                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,

                                                
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
                
               
               
               
                                                  ProfileImages = pImages5
                                              };
                                              userProfile5.DateBirth = new DateTime(1967, 3, 8);
                                              userProfile5.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile5.DateBirth);
                                              userProfile5.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile5.DateBirth);
                                             // userProfile5.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\Richard.jpg");
                                              db.Set<Profile>().Add(userProfile5);


                                              //====================================================================
                                              List<ProfileImage> pImages6 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "genrih8tyudor.jpg");
                                              pImages6.Add(pImage);
           

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "genrih81.jpg");
                                              pImages6.Add(pImage);
            

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 };//3
                                              pImage.SaveImageFromDisc(path + "genrih8.jpg");
                                              pImages6.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);



                                             

                                            
                                              Profile userProfile6 = new Profile()
                                              {
                                               
                                               
                                                  Id = 6,
                                                  MainProfileImageId = 6,
                                                  FirstName = "Henry",
                                                  LastName = "Tudor",
                                                  DateBirth = DateTime.Today,
                                                  StatusId = 1,
                                                  GenderValue = Gender.Men,
                                                  LookingForValue = Gender.Women,
                                                 
                                                  EMail = "HenryVIII@yahoo.com",
                                                  Phone = "(718) 646-6753",
                                                  AboutMyself = "Scholarly interests included writing both books and music, and I'm a lavish patron of the arts. ",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),


                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                              
                                                 // OccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,


                                                  CountryId = 195,
                                                  StateId = 33,
                                                  City = "NYC",
                                                  ZipCode = "11243",

                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,

                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,

                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,

                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,


                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,

                
                                                  SearchCountryId = 194,
                                                  SearchStateId = 0,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11003",

             
                
               
                
                                                  ProfileImages = pImages6
                                              };
                                              userProfile6.DateBirth = new DateTime(1957, 2, 2);
                                              userProfile6.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile6.DateBirth);
                                              userProfile6.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile6.DateBirth);
                                              //userProfile6.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\genrih8tyudor.jpg");
                                              db.Set<Profile>().Add(userProfile6);
                                              //--------------------------------------------------------------------------------------------------
         List<ProfileImage> pImages7 = new List<ProfileImage>();
                             pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                             pImage.SaveImageFromDisc(path + "71.jpg");
                             pImages7.Add(pImage);
                             db.Set<ProfileImage>().Add(pImage);

                             pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                             pImage.SaveImageFromDisc(path + "72.jpg");
                             pImages7.Add(pImage);
                             db.Set<ProfileImage>().Add(pImage);

                           

                             Profile userProfile7 = new Profile()
                                                                          {
                                                                              Id = 7,
                                                                              MainProfileImageId = 7,
                                                                              FirstName = "Philip",
                                                                              LastName = "Habsburg",
                                                                              DateBirth = DateTime.Today,

                                                                              GenderValue = Gender.Men,

                                                                              LookingForValue = Gender.Women,

                                                                              EMail = "PhilipII@yahoo.com",
                                                                              Phone = "(718) 436-6753",
                                                                              AboutMyself = "Philip II was King of Spain from 1556 and of Portugal from 1581 . From 1554 he was King of Naples and Sicily as well as Duke of Milan. During his marriage to Queen Mary I (1554–58), he was also King of England and Ireland.",
                                                                              SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                                              SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                                              SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                                              SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                                              + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                                              SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                                              SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                                              SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                                              RaceId = 1,
                                                                              OccupationId = 1,
                                                                              IncomeId = 6,
                                                                              SearchIncomeIdFrom = 4,
                                                                              SearchIncomeIdTo = 8,
                                                                              CountryId = 1,
                                                                              StateId = 1,
                                                                              City = "Brooklyn",
                                                                              ZipCode = "11243",
                                                                              HeightId = 5,
                                                                              SearchHeightIdFrom = 4,
                                                                              SearchHeightIdTo = 45,
                                                                              WeightId = 22,
                                                                              SearchWeightIdFrom = 20,
                                                                              SearchWeightIdTo = 40,
                                                                              AgeId = 5,
                                                                              SearchAgeIdFrom = 4,
                                                                              SearchAgeIdTo = 24,
                                                                              ChildrenId = 2,
                                                                              SearchChildrenIdFrom = 1,
                                                                              SearchChildrenIdTo = 4,
                                                                              SmokeId = 1,
                                                                              DisabilityId = 1,
                                                                              EducationGradeId = 2,
                                                                              ReligionId = 1,
                                                                              StatusId = 2,
                                                                              SearchCountryId = 1,
                                                                              SearchStateId = 1,
                                                                              SearchCity = "Brooklyn",
                                                                              SearchZipCode = "11243",
                                                                              ProfileImages = pImages7
                                                                          };
                                            
                                              userProfile7.DateBirth = new DateTime(1969, 1, 8);
                                              userProfile7.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile7.DateBirth);
                                              userProfile7.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile7.DateBirth);
                                          
                                              db.Set<Profile>().Add(userProfile7);
        //---------------------------------------------------------------------------------
                                              List<ProfileImage> pImages8 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "82.jpg");
                                              pImages8.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "81.jpg");
                                              pImages8.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                                              pImage.SaveImageFromDisc(path + "84.jpg");
                                              pImages8.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);


                                              pImage = new ProfileImage() { Id = 4, ImageOrderId = 4 }; //4
                                              pImage.SaveImageFromDisc(path + "83.jpg");
                                              pImages8.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              Profile userProfile8 = new Profile()
                                              {
                                                  Id = 8,
                                                  MainProfileImageId = 8,
                                                  FirstName = "Rudolf",
                                                  LastName = "Habsburg",
                                                  DateBirth = DateTime.Today,

                                                  GenderValue = Gender.Men,
                                                  LookingForValue = Gender.Women,

                                                  EMail = "Rudolf@yahoo.com",
                                                  Phone = "(718) 436-6753",
                                                  AboutMyself = "Rudolf, who was Archduke of Austria and Crown Prince of Austria-Hungary, was the heir apparent to the Austro-Hungarian Empire from birth. In 1889, he died in a suicide pact with his mistress, Baroness Mary Vetsera, at the Mayerling hunting lodge. The ensuing scandal made international headlines and remains a cause of speculation more than a century later.",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,
                                                  CountryId = 1,
                                                  StateId = 1,
                                                  City = "Brooklyn",
                                                  ZipCode = "11243",
                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,
                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,
                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,
                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,
                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,
                                                  StatusId = 2,
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
                                                  ProfileImages = pImages8
                                              };

                                              userProfile8.DateBirth = new DateTime(1989, 11, 2);
                                              userProfile8.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile8.DateBirth);
                                              userProfile8.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile8.DateBirth);

                                              db.Set<Profile>().Add(userProfile8);
        //--------------------------------------------------------------------------------------------------
                                              List<ProfileImage> pImages9 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "91.jpg");
                                              pImages9.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "92.jpg");
                                              pImages9.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                                              pImage.SaveImageFromDisc(path + "93.jpg");
                                              pImages9.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);




                                              Profile userProfile9 = new Profile()
                                              {
                                                  Id = 9,
                                                  MainProfileImageId = 9,
                                                  FirstName = "Mary",
                                                  LastName = "Vetsera",
                                                  DateBirth = DateTime.Today,

                                                  GenderValue = Gender.Women,
                                                  LookingForValue = Gender.Men,

                                                  EMail = "Vetsera@yahoo.com",
                                                  Phone = "(212) 436-6753",
                                                  AboutMyself = "Baroness Marie Alexandrine von Vetsera was a member of Austrian 'second society'  and one of Crown Prince Rudolf of Austria's mistresses. Vetsera and Rudolf were found dead, an apparent murder suicide, at his hunting lodge, Mayerling.",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,
                                                  CountryId = 1,
                                                  StateId = 1,
                                                  City = "Brooklyn",
                                                  ZipCode = "11243",
                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,
                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,
                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,
                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,
                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,
                                                  StatusId = 2,
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
                                                  ProfileImages = pImages9
                                              };

                                              userProfile9.DateBirth = new DateTime(1997, 11, 2);
                                              userProfile9.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile9.DateBirth);
                                              userProfile9.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile9.DateBirth);

                                              db.Set<Profile>().Add(userProfile9);
        //---------------------------------------------------------------------------------------------
                                              List<ProfileImage> pImages10 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "101.jpg");
                                              pImages10.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "102.jpg");
                                              pImages10.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                                              pImage.SaveImageFromDisc(path + "103.jpg");
                                              pImages10.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 4, ImageOrderId = 4 }; //4
                                              pImage.SaveImageFromDisc(path + "104.jpg");
                                              pImages10.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);




                                              Profile userProfile10 = new Profile()
                                              {
                                                  Id = 10,
                                                  MainProfileImageId = 10,
                                                  FirstName = "George",
                                                  LastName = " Buckingham",
                                                  DateBirth = DateTime.Today,

                                                  GenderValue = Gender.Men,
                                                  LookingForValue = Gender.Women,

                                                  EMail = "Buckingham@yahoo.com",
                                                  Phone = "(212) 436-6753",
                                                  AboutMyself = "George Villiers, 1st Duke of Buckingham .",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,
                                                  CountryId = 1,
                                                  StateId = 1,
                                                  City = "Brooklyn",
                                                  ZipCode = "11243",
                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,
                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,
                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,
                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,
                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,
                                                  StatusId = 2,
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
                                                  ProfileImages = pImages10
                                              };

                                              userProfile10.DateBirth = new DateTime(1977, 2, 2);
                                              userProfile10.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile10.DateBirth);
                                              userProfile10.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile10.DateBirth);
                                              db.Set<Profile>().Add(userProfile10);
        //------------------------------------------------------------------------------------------------------------------------
                                              List<ProfileImage> pImages11 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "111.jpg");
                                              pImages11.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "112.jpg");
                                              pImages11.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                                              pImage.SaveImageFromDisc(path + "113.jpg");
                                              pImages11.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 4, ImageOrderId = 4 }; //4
                                              pImage.SaveImageFromDisc(path + "114.jpg");
                                              pImages11.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 5, ImageOrderId = 5 }; //4
                                              pImage.SaveImageFromDisc(path + "115.jpg");
                                              pImages11.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);



        //-----------------------------------------------------------------------------------------------------------
                                              Profile userProfile11 = new Profile()
                                              {
                                                  Id = 11,
                                                  MainProfileImageId = 11,
                                                  FirstName = "Anne",
                                                  LastName = "Habsburg",
                                                  DateBirth = DateTime.Today,

                                                  GenderValue = Gender.Women,
                                                  LookingForValue = Gender.Men,

                                                  EMail = "AnnaHabsburg@yahoo.com",
                                                  Phone = "(212) 436-6753",
                                                  AboutMyself = "Anne of Austria was queen consort of France and Navarre, regent for her son, Louis XIV of France, and a Spanish and Portuguese Infanta by birth. During her regency , Cardinal Mazarin served as France's chief minister. ",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,
                                                  CountryId = 1,
                                                  StateId = 1,
                                                  City = "Brooklyn",
                                                  ZipCode = "11243",
                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,
                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,
                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,
                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,
                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,
                                                  StatusId = 2,
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
                                                  ProfileImages = pImages11
                                              };

                                              userProfile11.DateBirth = new DateTime(1987, 6, 2);
                                              userProfile11.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile11.DateBirth);
                                              userProfile11.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile11.DateBirth);

                                              db.Set<Profile>().Add(userProfile11);

                                             


        //-----------------------------------------------------------------------------------------

                                              List<ProfileImage> pImages12 = new List<ProfileImage>();
                                              pImage = new ProfileImage() { Id = 1, ImageOrderId = 1 }; //1
                                              pImage.SaveImageFromDisc(path + "121.jpg");
                                              pImages12.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 2, ImageOrderId = 2 };//2
                                              pImage.SaveImageFromDisc(path + "122.jpg");
                                              pImages12.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 3, ImageOrderId = 3 }; //3
                                              pImage.SaveImageFromDisc(path + "123.jpg");
                                              pImages12.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 4, ImageOrderId = 4 }; //4
                                              pImage.SaveImageFromDisc(path + "124.jpg");
                                              pImages12.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);

                                              pImage = new ProfileImage() { Id = 5, ImageOrderId = 5 }; //4
                                              pImage.SaveImageFromDisc(path + "125.jpg");
                                              pImages12.Add(pImage);
                                              db.Set<ProfileImage>().Add(pImage);



        
                                              Profile userProfile12 = new Profile()
                                              {
                                                  Id = 12,
                                                  MainProfileImageId = 12,
                                                  FirstName = "Joanna",
                                                  LastName = "Trastamara",
                                                  DateBirth = DateTime.Today,

                                                  GenderValue = Gender.Women,
                                                  LookingForValue = Gender.Men,

                                                  EMail = "Joanna@yahoo.com",
                                                  Phone = "(212) 436-6753",
                                                  AboutMyself = "Joanna, known as Joanna the Mad , was queen of Castile from 1504 and of Aragon from 1516. From the union of these two crowns modern Spain evolved. Joanna married Philip the Handsome, who was crowned King of Castile in 1506, initiating the rule of the Habsburgs in Spain. After Philip's death that same year, Joanna was deemed mentally ill and was confined to a nunnery for the rest of her life.",
                                                  SearchStatusIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  SearchRaceIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchOccupationIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchReligionIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)
                                                  + Math.Pow(2, 6) + Math.Pow(2, 8) + Math.Pow(2, 9)),
                                                  SearchDisabilityIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 4)),
                                                  SearchSmokeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 3)),
                                                  SearchEducationGradeIdMask = (long)(Math.Pow(2, 1) + Math.Pow(2, 2) + Math.Pow(2, 3) + Math.Pow(2, 4) + Math.Pow(2, 5)),
                                                  RaceId = 1,
                                                  OccupationId = 1,
                                                  IncomeId = 6,
                                                  SearchIncomeIdFrom = 4,
                                                  SearchIncomeIdTo = 8,
                                                  CountryId = 1,
                                                  StateId = 1,
                                                  City = "Brooklyn",
                                                  ZipCode = "11243",
                                                  HeightId = 5,
                                                  SearchHeightIdFrom = 4,
                                                  SearchHeightIdTo = 45,
                                                  WeightId = 22,
                                                  SearchWeightIdFrom = 20,
                                                  SearchWeightIdTo = 40,
                                                  AgeId = 5,
                                                  SearchAgeIdFrom = 4,
                                                  SearchAgeIdTo = 24,
                                                  ChildrenId = 2,
                                                  SearchChildrenIdFrom = 1,
                                                  SearchChildrenIdTo = 4,
                                                  SmokeId = 1,
                                                  DisabilityId = 1,
                                                  EducationGradeId = 2,
                                                  ReligionId = 1,
                                                  StatusId = 2,
                                                  SearchCountryId = 1,
                                                  SearchStateId = 1,
                                                  SearchCity = "Brooklyn",
                                                  SearchZipCode = "11243",
                                                  ProfileImages = pImages12
                                              };

                                              userProfile12.DateBirth = new DateTime(1987, 8, 6);
                                              userProfile12.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(userProfile12.DateBirth);
                                              userProfile12.ZodiacSignId = ZodiacSign.GetZodiacSignId(userProfile12.DateBirth);

                                              db.Set<Profile>().Add(userProfile12);
        //-------------------------------------------------------------------------------------------------

                                              db.SaveChanges();
                          
             //--------------------------------------------
            NumeralogyNumber numeralogy;

           numeralogy = new NumeralogyNumber() { Number1 = 1, Number2 = 1 , //11
              Info = "Two people with a strong desire to lead, and, two people who want very much to be independent. Two 1s in a relationship understand and accept each other perhaps better than any other number can understand the 1. This is a relationship not without pitfalls as the match can get dicey when they start to compete. But for the most part, it is a good one filled with excitement and activity."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //12
           {
               Number1 = 1,
               Number2 = 2,
               Info = "Two very different people who do well if they remember their roles. The 1 is best equipped to be the breadwinner, and the 2 will be the one to feather the nest and keep the warmth of romance alive and well. The 1 must avoid being distracted and never forget how important attention is for the 2 partner."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //13
           {
               Number1 = 1,
               Number2 = 3,
               Info = "This is a very lively couple that seem to shamelessly enjoy life and each other. The 3 is good at acknowledging the 1 accomplishments and stroking the ego. The 3 provides the ideas and the 1 provides the push, so this couple can cover a lot of ground. Yet they have to be careful about what they say since neither handles criticism very well."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy); 

           numeralogy = new NumeralogyNumber() //14
           {
               Number1 = 1,
               Number2 = 4,
               Info = "4's desire for control is a tough sell on the 1. The 1's need to make things happen now, frustrates the meticulous 4. Opposites in many ways, these two should not expect easy compromise. If they ever can accept one another for who they are, their respective strengths will make a solid relationship."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //15
           {
               Number1 = 1,
               Number2 = 5,
               Info = "This is a very compatible combination as both of these numbers are ones that like to have a lot of freedom in a relationship. The drive for independence is paramount for each. They may be so busy with their own 'thing' that time together is limited, very special, and often exciting. As you might expect, the most serious threat is when either tries to impose his or her will on the other."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //16
           {
               Number1 = 1,
               Number2 = 6,
               Info = "This is a power struggle waiting to happen. The 6 wants and needs to a caretaker. The 1 has an absolute need to be independent and unrestrained. This can be a successful pairing only if they can work past this roadblock, and give each other the support they both need."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //17
           {
               Number1 = 1,
               Number2 = 7,
               Info = "These are very different energies but ones that somehow blend nicely. The 7 provides wise insights while the 1 becomes a needed motivator. The key is to understand the tendencies; the 1 can get too busy with the outer world to always be there for the 7, and the 7 can be too into their own world to be there for the 1. Neither should take this absence personally."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy); //18

           numeralogy = new NumeralogyNumber()
           {
               Number1 = 1,
               Number2 = 8,
               Info = "From a business standpoint, this is a good match. But from the love angle, it is questionable at best. Both are so assertive and demanding, that expectations can far exceed reality. Negative feedback from either will be deadly in this pairing. Success depends on a open and mutual willingness to compromise and limit demands."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber()//19
           {
               Number1 = 1,
               Number2 = 9,
               Info = "The 9 brings a selflessness to the relationship which allows the 1 to operate in an environment that is understanding and smooth flowing. The 1 will have to learn to share the partner who is inclined to extend a giving nature outside the home. If there is trouble in this partnership it will generally come from the 9s difficulty tolerating the 1's assertive and individualistic behavior."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

        //------------------------------------------------------

           numeralogy = new NumeralogyNumber() //22
           {
               Number1 = 2,
               Number2 = 2,
               Info = "A great match of two souls both needing to give and receive love. Experts at mediation, they have little difficulty finding common ground on just about any issue that arises. The only word of caution for this pairing is that they must each remember how thin their own skin is so as to not cause verbal injury to the other. Generally this is not a problem because of their polite manner and mutual respect."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber()//23
           {
               Number1 = 2,
               Number2 = 3,
               Info = "These are potentially very good partners because of the good humor and good chemistry. The 3 is always 'on stage' and full of life and social energy, while the 2 is happy as a lark standing back and enjoying the show. The 2 balances the needs of the 3 by providing a soothing and calming influence."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //24
           {
               Number1 = 2,
               Number2 = 4,
               Info = "This is a steady pairing resulting in comfort personified. When it comes to home and family, the 4 is the ultimate builder and provider. Security is 4's forte. Nothing is more appealing to the 2 than home, hearth, and family. The only difficulty likely here is one of perception. The 2 needs love to be shown and always physically apparent, and the 4 is sometimes not so demonstrative."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //25
           {
               Number1 = 2,
               Number2 = 5,
               Info = "The 2 needs family and an ever present sense of being loved, and the 5 need total freedom to pursue whatever avenues appear on the horizon. This is one where the chemistry has to be very strong in order for the two very different souls to forge some significant compromises. Obviously, these two can provide a lot that may otherwise be missing, but it won't be an easy road."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //26
           {
               Number1 = 2,
               Number2 = 6,
               Info = "This is another good love match. The 6 ranks first in family while the 2 tops the chart in love and caring. Still the pair need to watch their Ps and Qs as the 6 has a surprising need for approbation, and 2's thin skin can suffer with the direct and demanding approach that sometimes characterizes the 6. Considerations of feelings is a must."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber()//27
           {
               Number1 = 2,
               Number2 = 7,
               Info = "This is a couple that has some strong and dissimilar needs. The 2's need for demonstrative love and 7's need for a good deal of space and solitude make this a pairing that will work only if both can stay tuned in to the other's needs and be willing to cater to them at least to some degree. Generally the 2 will have to find something to occupy much of the time that would otherwise be devoted to the mate."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber()//28
           {
               Number1 = 2,
               Number2 = 8,
               Info = "A pairing that usually works very well because it is likely that each has a clear vision of their role. The 8 is about the outer world of business and attainment, taking care of the financial needs of the family. The 2 takes care of the family and is there to pamper the ego of their partner. A pitfall in this relationship can occur if the 8 fails to sufficiently value the labors of the 2. Generally, this is the classic traditional family model, or in the case of the male 2, female 8, the classic male/female role reversal."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber()//29
           {
               Number1 = 2,
               Number2 = 9,
               Info = "These two can have a wonderful relationship or it can be not so wonderful. The 2 needs constant attention, and certainly the 9 is a caring individual. But the care they naturally possess is spread to all humanity and often it is not focused enough at home. The 9 is a natural leader and the 2 is a natural follower, so there is always hope. The 9 needs to remember that the 2 hates to be alone, and the 2 needs to be forewarned that the 9's love will only stretch so far."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
        //------------------------------------------------------------------

           numeralogy = new NumeralogyNumber() //33
           {
               Number1 = 3,
               Number2 = 3,
               Info = "Wild and interesting describes this pairing of two with so much creative and social potential. No one has more fun that a pair of 3s who understand and support one another. The question may become who is going to take care of the mundane. The pitfall of this relationship comes when neither partner can hold on to reins of practical everyday details."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //34
           {
               Number1 = 3,
               Number2 = 4,
               Info = "When the spontaneous 3 pairs with the micro-manager 4, something has to give, and often it won't. The 3 will take each day as it comes while the 4 has to have a definite plan far into the future. If the two can ever figure out how to meet in the middle, they will do a good job of balancing each other's shortcomings. The 3 will show the 4 how to have fun while the 4 can give the 3 a needed sense of security."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //35
           {
               Number1 = 3,
               Number2 = 5,
               Info = "This is one of the most social combination you will find. The two will generally find each other very interesting and their ability to entertain will be never ending. Social opportunities, travel, and numerous activities promise this relationship won't get boring. Both are creative by nature, yet neither excels at managing the budget, so everyday affairs can cause problems sometimes."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //36
           {
               Number1 = 3,
               Number2 = 6,
               Info = "This is a natural combination that works well in most cases. The 3 is full of enthusiasm and ideas, and the 6 provides the stability, support, and encouragement that often makes this combination an idea team in many ways. The chemistry here is very strong and durable. The challenge of this combination can come in the form of 6 jealous feelings toward the oft flirtatious 3. Usually it will be the 6 who will have to learn to deal with an inborn trait."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //37
           {
               Number1 = 3,
               Number2 = 7,
               Info = "These two are about as different as people get. The 3 wants to be constantly on the go with a swirl of activity, travel, and social contact that the 7 will find intolerable. The 7 needs solitude and can only take so much human contact before retreating to their preferred peace and quiet. Confrontation in this pairing never works well, and it will be up to both to understand the long-term need for compromise. The key to success here is open dialog regarding wants, needs, and goals."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //38
           {
               Number1 = 3,
               Number2 = 8,
               Info = "This is a combination that requires extra effort mostly because of the departure of needs. The 8 needs goals and authority to feel happy, and efforts focused here leave the 3 without needed attention and stimulation. The secret to success in this combination, if there is one, is frequent getaways; periods when the 3 gets the 8 away from business and relaxed enough to have some fun. There isn't much frivolity in the 8, and this can sometimes be a tough sale."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //39
           {
               Number1 = 3,
               Number2 = 9,
               Info = "This is a wonderful combination of two people who are apt to keep each other endlessly engaged in a variety of creative ways. Both like to be on stage and both are interested in people. They care about people and the 9 can be generous to a fault. The 9 is the teacher and the 3 is the ever eager student. Sharing experience is a never ending joy of this pairing. The problem the couple faces is settling down, feathering a nest, and keeping the bills paid. Even after they are settled and set, romantic adventures will always be important."
           };

          //-----------------------------------------------------------------------
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //44
           {
               Number1 = 4,
               Number2 = 4,
               Info = "The keywords for this combination are solid and secure. For individuals who need to know that the bills are paid and future is totally secure, who better to fill this need than another 4. These two will share goals that they work for and nearly always achieve. Success is measured by a sense that growth is continual and this includes love and romance. The down side of this pairing, if there is one, is the sense that nothing is ever completely okay. It's hard to relax , be spontaneous, and enjoy the moment and each other. Nonetheless, there are few relationships destined to be more stable than this one."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //45
           {
               Number1 = 4,
               Number2 = 5,
               Info = "Challenge. The 4 and the 5 have different temperaments and different ways of communicating. The 4 is very direct to the point, while the 5 will be more diplomatic and indirect. 4s don't like change, and 5s have to have it. To find success, this couple will need to respect the difference and pay attention to what is said and what is inferred."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //46
           {
               Number1 = 4,
               Number2 = 6,
               Info = "A comfortable match of traditional types, this pairing has promise from the very beginning. Chances are the 6 will want to take the lead in this relationship, and a secure home and family are all that is needed to produce a satisfying long-term situation. The challenge of this relationship is compromise. Neither is very good at this."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //47
           {
               Number1 = 4,
               Number2 = 7,
               Info = "This is a relationship that has a serious tone about it and may be driven by a mutual need for security. It is loyal and devoted, even it it may lack the fire and passion of some combinations. The 4 is a natural provider of rock solid security and home values. The 7 provides a quest for continued mental development and adventure. The combination makes life both secure and yet more interesting for both."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //48
           {
               Number1 = 4,
               Number2 = 8,
               Info = "This is a comfortable pairing because both parties understand hard work and have a good head for business and getting ahead in the world. The 4 is the cautious planner, while the 8 has a more grandiose approach to endeavors. The only problem probably can be one of finding the time to spend together in a quest for romance. Yet this is a couple that knows how to build for the future and develop a very secure relationship."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //49
           {
               Number1 = 4,
               Number2 = 9,
               Info = "These two are so very different that successful pairings are rare. The 9 is far more social and fosters deep humanitarian instincts. The 4 is focused on the basics of building a secure and solid immediate world. To succeed in a relationship requires both to be aware of how different their approaches are, and somehow muster a willingness to accept. The 4 will certainly appreciate the 9's knowledge and intelligence, and the 9 must likewise value the 4's ruthless consistency."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

        //-------------------------------------------------------------------------------------------
           numeralogy = new NumeralogyNumber() //55
           {
               Number1 = 5,
               Number2 = 5,
               Info = "A pair of 5s is a very compatible couple who prize their freedom to be different and adventuresome. Few others will be as open to the constant flow of new ideas and changes so common in the 5. In this relationship, partners easily anticipate what the other is thinking and where they are going. And they also have a sense for staying out of the way and accepting the freewheeling lifestyle. 5s choosing to support one another can do just about anything. The difficulty with this pairing is focus, and there may be a problem handling the mundane day-to-day affairs."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //56
           {
               Number1 = 5,
               Number2 = 6,
               Info = "This is a combination requiring great compromise since the 5 thrives on freedom and space, and the 6 is noted for exerting control and nurturing supervision. The 6 wants complete commitment, and the 5 is looking for adventure and new horizons. In a relationship, they would probably be good for each other if they could find a way to meet somewhere near the middle. They must avoid any tendency to become entrenched in positions that just won't work for the other."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);


           numeralogy = new NumeralogyNumber() //57
           {
               Number1 = 5,
               Number2 = 7,
               Info = "This is a relationship which is more or less free of rules and procedures. In some ways, these two are much alike and the relationship is generally very compatible. The 7 values the time to be alone and enjoy the world of study and reflection in their private space. At the same time, the 5 has plenty going on and appreciates not having demands for attention being the paramount feature of the relationship. Yet these two can get together and find a never ending stream of mutual interests to discuss and explore."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);


           numeralogy = new NumeralogyNumber() //58
           {
               Number1 = 5,
               Number2 = 8,
               Info = "This is a relationship between two individuals who don't always follow the rules, and they might find themselves locking horns on the rules of a relationship. The 8 is used to being the boss and dominating most situations. The 5 seeks freedom from any restraints. The 8 is focused on success, particularly in a financial sense, and the 5 doesn't even want to think about money. It will take careful planning and compromise to make this relationship work."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);



           numeralogy = new NumeralogyNumber() //59
           {
               Number1 = 5,
               Number2 = 9,
               Info = "This is a relationship between two who may find it hard to work the relationship into their busy schedules. Both of these numbers represent people who are apt to be in a constant state of transition and change. In this regard they have much in common and will generally find each other very interesting, for the moment or for the long haul. The compassion of the 9 and progressive thinking of the 5 seems to blend well. Establishing a commitment to security is a must."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
        //--------------------------------------------------

           numeralogy = new NumeralogyNumber() //66
           {
               Number1 = 6,
               Number2 = 6,
               Info = "This is a combination charged with romance, but in essence it is rather practical by nature. Home and family is second nature here, and these will be the top priorities for sure. This is a very compatible pairing. The 6 knows what's best for their partner, so they do a good job of taking care of each other, and a family is a must. Yet the 6 by its nature wants the whole family under his/her thumb, so the challenge may be in agreeing who is going to be charge. The circumstances of the relationship will usually be able to sort this out."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);

           numeralogy = new NumeralogyNumber() //67
           {
               Number1 = 6,
               Number2 = 7,
               Info = "These are two very different people with very different ideas about a relationship. The 6 is openly interested in a permanent situation complete with a stable home and family. With the secretive 7, it is hard to tell what the goal might be, and only time will tell. Despite the sexual attraction that may be present, this is a very challenged combination. The 6 is too controlling, and the 7 just isn't to be closely managed. The compromises required in this relationship really bend and manipulate the natural traits of both numbers."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //68
           {
               Number1 = 6,
               Number2 = 8,
               Info = "This is a very positive and compatible relationship of two who are usually open and positive in most that they do. This is a couple with big ideas, and their ideas are usually brought to reality in grand fashion. The home will provide plenty of space for family, work, and frequent entertaining of their many friends. A down side to the relationship can occur if the possessive 6 has to compete too much with the business interests of the 8. Likewise, the 8 will be frustrated when the demands at home cramp the executive lifestyle and obligations."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //69
           {
               Number1 = 6,
               Number2 = 9,
               Info = "This is generally a very compatible relationship prospect as the 9 is one of the few numbers to gain 6's utmost respect. In a family situation, the 6 is unsurpassed as a manager, and the 9 is never reticent in heaping praise in recognition. This often creates a mutual admiration environment that provides a happy home for both partners. The 6 helps the 9 stay focused on details and common sense issues, while the 9 broadens the 6's outlook and sense of the world at large. The expansiveness of this pairing may suggest the need to keep a close eye on the budget."
           };
        //----------------------------------------------------------


           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //77
           {
               Number1 = 7,
               Number2 = 7,
               Info = "This is one situation in which no one understands the eccentricities of a 7 nearly as well as another 7. Thus, this is a very compatible pairing. With the right attitude, this couple will find the interest to freely explore the world together, or spend their days in happy solitude together. Chances are you are on the same psychic wavelength, so you will surely catch the signals as they flash by. The downside of this pairing is the tendency to not communicate, so an effort may have to be made to keep the lines open and operating."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //78
           {
               Number1 = 7,
               Number2 = 8,
               Info = "This is a combination that seems to work well physically, but one which is plagued with problems on the emotional level. The 8 has a tendency to dominate and control, and the 7 is a very private person prone to resist attempts to exert authority. The power of both numbers can result in frequent verbal battles. Yet there is a stability factor in the pairing that makes long-term success a possibility if accommodations can be reached."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //79
           {
               Number1 = 7,
               Number2 = 9,
               Info = "This couple is anything but neutral in their affiliation. They are listed neutral because the relationship can go either way depending largely on spiritual beliefs. Both have strong spiritual inclinations and deeply held positions. However, these views of the spiritual and God often divide along rigidly held lines. In such cases, compromise is usually not an option. When the spiritual positions are in harmony, this can be a very compatible combination."
           };
          //----------------------------------------------------
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //88
           {
               Number1 = 8,
               Number2 = 8,
               Info = "This pairing might be known as the 'Dynamic Duo' as this is a combination that is full of passion and romance. Indeed, this pairing is a strong and enduring match- up. Yet both partners will be easily distracted by the events in their life as goals and professional demands often supersede romantic possibilities. Solid as the relationship probably is, the couple may have a hard time communicating the depth of feeling in either word or deed. Guard against competing with one another, and getting caught up in schedules and the demands of work. Make time for each other and always focus on being equal partners."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           numeralogy = new NumeralogyNumber() //89
           {
               Number1 = 8,
               Number2 = 9,
               Info = "This is a challenging combination in which two highly motivated individuals - motivated in very different ways - find it hard to accept the ways of the other. In general the goals of the 9 are lofty and may have a humanitarian bend, while the 8 seeks the reward that comes with development of leadership and material success. To have much of a chance, the 8 will have to appreciate the lessons that can be learned from their generous partner. When they are able to work as a team, this is a powerful and often inspirational pair. Too often, however, the combination fails to click."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
        //----------------------------------------------------
           numeralogy = new NumeralogyNumber() //99
           {
               Number1 = 9,
               Number2 = 9,
               Info = "This relationship has much promise as it usually engages two charismatic types possessing much intellectual stimulation. The nature of the 9 is selfless, so this combination usually faces few challenges; each wanting to please the other and always be there for each other. A relationship offering the opportunity to grow, learn, and serve, this is often an inspiring combination."
           };
           db.Set<NumeralogyNumber>().Add(numeralogy);
           



           db.SaveChanges();
           
        }

        //--------------------------------------------------------


      







    //-----------------------------------------------------------
    private static void SetInitData<T>(string[] ar, DbContextEF db) where T : class, IAttribute, new()
    {

        for (int i = 0; i < ar.Length; i++)
        {
            T p = new T() { Id = i + 1, Name = ar[i] };
            db.Set<T>().Add(p);

        }
    }

    //----------------------------------------------------------
    private static void SetInitDataForState(InitStateNames[] ar, DbContextEF db)
    {

        for (int i = 0; i < ar.Length; i++)
        {
            State p = new State() { Id = i + 1, Name = ar[i].Name2 };
            db.Set<State>().Add(p);
        }
    }


    // "{0} - {1}\""
    //--------------------------------------------------------
    private static void SetInitDataForIncrement<T>(string template,
                                             int startValue, int endValue,
                                             int increment,
                                             DbContextEF db) where T : class, IAttribute, new()
    {

        for (int i = startValue; i < endValue; i += increment)
        {

            T p = new T() { Id = i + 1, Name = String.Format(template, i) };
            db.Set<T>().Add(p);
        }

    }


    private static void SetInitDataForRange<T>(string templateRange,
                                                  int startValue, int endValue,
                                                  int increment,
                                                  DbContextEF db)
                                                  where T : class, IAttribute, new()
    {

        for (int i = startValue; i < endValue; i += increment)
        {

            T p = new T() { Id = i + 1, Name = String.Format(templateRange, i, i + increment) };
            db.Set<T>().Add(p);
        }

    }
    //-------------------------------------------------------------------
    private static void SetInitDataForRangeInt<T, TT>(
                                                    string template, string templateRange, string lastString,
                                                           int startValue, int endValue,
                                                           int increment, int rangeIncrement,
                                                           DbContextEF db)
        where T : class, IAttribute, IRangeFilter, new()
        where TT : class, IAttribute, new()
    {
        int j = 1;
        T p;
        TT pp;

        for (int i = startValue; i <= endValue; i += increment)
        {

            if (i == endValue)
                p = new T() { Id = i + 1, RangeId = j, Name = String.Format(template, "Up", "") };

            else
                p = new T() { Id = i + 1, Name = String.Format(template, i, lastString), RangeId = j };

            db.Set<T>().Add(p);


            if ((i != startValue) && (i - startValue) % rangeIncrement == 0)
            {

                if (i + rangeIncrement > endValue)
                    pp = new TT() { Id = ++j, Name = String.Format(templateRange, i - rangeIncrement, " Up", "") };
                else
                {
                    pp = new TT() { Id = ++j, Name = String.Format(templateRange, i - rangeIncrement, i - 1, lastString) };
                    if (increment == 1) { p.RangeId = pp.Id; }
                }
                db.Set<TT>().Add(pp);

            }
        }

    }

    ////--------------------------------------------------------------------------------------
    private static void SetInitDataForRangeInt1<T, TT>(
                                               string template, string templateRange,
                                                      int startValue, int endValue,
                                                      int increment, int rangeIncrement,
                                                      DbContextEF db)
        where T : class, IAttribute, new()
        where TT : class, IAttribute, new()
    {
        int j = 1;

        for (int i = startValue; i < endValue; i += increment)
        {

            T p = new T() { Id = i + 1, Name = String.Format(template, i) };
            db.Set<T>().Add(p);
            // add j to p

            if ((i != startValue) && (i - startValue) % rangeIncrement == 0)
            {
                TT pp = new TT() { Id = ++j, Name = String.Format(templateRange, i - rangeIncrement, i - 1) };
                db.Set<TT>().Add(pp);

            }
        }

    }

    //--------------------------------------------------------------------------------------
    private static void SetInitDataForRangeDouble<T, TT>(
                                               string template, string templateRange, string lastString,
                                                      int startValue, int endValue,
                                                      int increment, int rangeIncrement,
                                                      DbContextEF db)
        where T : class, IAttribute, IRangeFilter, new()
        where TT : class, IAttribute, new()
    {
        int j = 1;
        TT pp;
        T p;

        for (int i = startValue; i <= endValue; i += increment)
        {

            if (i == endValue)
                p = new T() { Id = i + 1, RangeId = j, Name = String.Format(template, "Up", "") };

            else
                p = new T() { Id = i + 1, RangeId = j, Name = String.Format(template, ((double)i) / 10, lastString) };
            db.Set<T>().Add(p);
            // add j to p

            if ((i != startValue) && (i - startValue) % rangeIncrement == 0)
            {

                if (i + rangeIncrement > endValue)
                    pp = new TT()
                    {
                        Id = ++j,
                        Name = String.Format(templateRange,
                            ((double)(i - rangeIncrement)) / 10, " Up", "")
                    };
                else
                {
                    pp = new TT()
                    {
                        Id = ++j,
                        Name = String.Format(templateRange,
                            ((double)(i - rangeIncrement)) / 10, ((double)(i - 1)) / 10, lastString)
                    };
                    if (increment == 1) { p.RangeId = pp.Id; }
                }
                db.Set<TT>().Add(pp);

            }
        }

    }

    //==================================================
    private static void SetInitDataForRangeDoubleDDDD<T, TT>(
                                                  string template, string templateRange,
                                                         double startValue, double endValue,
                                                         double increment, double rangeIncrement,
                                                         DbContextEF db)
        where T : class, IAttribute, new()
        where TT : class, IAttribute, new()
    {
        int j = 1;

        for (double i = startValue; i < endValue; i += increment)
        {

            T p = new T() { Id = (int)i + 1, Name = String.Format(template, i) };
            db.Set<T>().Add(p);
            // add j to p

            if ((i != startValue) && (((int)(i * 10 - startValue * 10)) % ((int)(rangeIncrement * 10)) == 0))
            {
                TT pp = new TT()
                {
                    Id = ++j,
                    Name = String.Format(templateRange,
                        i - rangeIncrement, i)
                };
                db.Set<TT>().Add(pp);

            }
        }

    }
        // 

     //------------------------------------------------------------
    }
}
