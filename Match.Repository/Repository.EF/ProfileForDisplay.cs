﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using Match.Repository.EF;
using Match.Domain.Entities;




namespace Match.Repository.EF
{
    public class ProfileForDisplay
    {

        public DateTime ClientDateBirth { get; set; }
        public Profile Profile { get; set; }
        public ZodiacSign zodiac { get; set; }

        public  Income Income { get; set; }
        public  Race Race { get; set; }
        public Occupation Occupation  { get; set; }

        public Status Status { get; set; }
        public Religion Religion { get; set; }
        public Country Country { get; set; }
        public State State { get; set; }
        public Height Height { get; set; }
        public Weight Weight { get; set; }
        public Smoke Smoke { get; set; }
        public Age Age { get; set; }
        public Children Children { get; set; }
        public Disability Disability { get; set; }

        [DisplayName("Education Grade")]
        public EducationGrade EducationGrade { get; set; }
    }
}
