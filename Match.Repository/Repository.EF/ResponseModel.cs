﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Match.Repository.Abstract;
using Match.Domain.Entities;
using Match.Repository.ContextStorage;

namespace Match.Repository.EF
{
    public class ResponseModel
    {
        public Profile Profile { get; set; }
        public IEnumerable<Profile> Profiles { get; set; }
        public int AllProfiles { get; set; }
        public int AllProfilesByNumerology { get; set; }
        public int isOpen { get; set; }

        public IEnumerable<Status> Statuses { get; set; }

        public IEnumerable<Income> Incomes { get; set; }
        public IEnumerable<Occupation> Occupations { get; set; }
        public IEnumerable<Race> Races { get; set; }
        
        public IEnumerable<Religion> Religions { get; set; }
        public IEnumerable<Country> Countries { get; set; }
        public IEnumerable<State> States { get; set; }
        public IEnumerable<Height> Heights { get; set; }
        public IEnumerable<SearchHeight> SearchHeights{ get; set; }
        public IEnumerable<Weight> Weights { get; set; }
        public IEnumerable<SearchWeight> SearchWeights { get; set; }
        public IEnumerable<Smoke> Smokes { get; set; }
        public IEnumerable<Age> Ages { get; set; }
        public IEnumerable<SearchAge> SearchAges { get; set; }
        public IEnumerable<Children> HasChildren { get; set; }
        public IEnumerable<Disability> Disabilities { get; set; }
        public IEnumerable<EducationGrade> EducationGrades { get; set; }
    }
}