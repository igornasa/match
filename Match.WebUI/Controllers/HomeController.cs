﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Entities;
using Match.Repository.EF;
using Match.WebUI.ViewModel;

namespace Match.WebUI.Controllers
{
    public class HomeController : Controller
    {
       
        //----------------------------------------
        public ActionResult Index(int id = 5)
        {

            ResponseModel r = new ResponseModel();
            r.Profile = new Profile();
            r.Profile.Id = id;
            return View(r);

        }

       //----------------------------------------
        public ActionResult Help(int id = 0)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Profile = new Profile();
            responseModel.Profile.Id = id;

            return View("Help", responseModel);
        }        


        //------------------------------------
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }


        //--------------------------------------
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //---------------------------------
        public ActionResult Match()
        {
            ViewBag.Message = "Matching";
            
            SimpleMatching m = new SimpleMatching();

            return View("SimpleMatch", m);
        }

    }
}
