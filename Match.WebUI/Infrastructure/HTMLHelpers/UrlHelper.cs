﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;




namespace Match.WebUI.Infrastructure.HTMLHelpers
{     // must to add <add namespace="ProjectMVC.WebUI.Infrastructure.HtmlHelpers.Resolve"/> 
    //Добавляем пространство имен вспомогательного метода HTML в файл  Views/Web.config

    public static class UrlHelper
    {
        public static string Resolve(string resource)
        {
            return string.Format("{0}://{1}{2}{3}",
                  HttpContext.Current.Request.Url.Scheme,
                  HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
                  (HttpContext.Current.Request.ApplicationPath.Equals("/")) ?
                            string.Empty : HttpContext.Current.Request.ApplicationPath,
                  resource);
        }
    }

}
