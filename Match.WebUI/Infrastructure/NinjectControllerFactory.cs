﻿
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using Ninject;
using System.Linq;
using Moq;
using Match.Domain.Entities;
using Match.Domain.Abstract;
using Match.Repository.Abstract;
using Match.Repository.EF;







namespace Match.WebUI.Infrastructure
{
    // add to global.asax
    // ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            //AddBindings();


            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext,
                                    Type controllerType)
        {
            // получение объекта контроллера из контейнера
            // используя его тип
            return controllerType == null
            ? null
            : (IController)kernel.Get(controllerType);
        }



        private void AddBindingsDB()
        {
            kernel.Bind<IRepositoryCRUD<Profile, int>>().To<ProfileRepository>();
            
        }
    }
}