﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Match.Repository.Abstract;
using Match.Domain.Entities;
using Match.Repository.EF;


namespace Match.WebUI.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IRepositoryCRUD<Profile, int> repository;

        public ProfileController(IRepositoryCRUD<Profile, int> repository)
            
        {
            this.repository = repository;
        }


       public FileContentResult GetImage(int id)
        {

            Profile f = repository.FindBy(id); 
            if (f == null || f.ImageData == null) return null;
            return File(f.ImageData, f.ImageMimeType);

        }

       public FileContentResult GetProfileImage(int imageId)
       {

           ProfileImage f = repository.FindProfileImageBy( imageId);
           if (f == null || f.ImageData == null) return null;
           return File(f.ImageData, f.ImageMimeType);

       }

        

        public ActionResult Index()
        {
            Profile p = new Profile();
           // p.Age = new Age { Id = 1 };


            ResponseModel responseModel = repository.FindAllFor(0);
            responseModel.Profile.DateBirth = DateTime.Today;
            responseModel.Profile.Id = 0;

            return View(responseModel);
        }



        public ActionResult Index1(int id = 5)
        {
            ResponseModel  r = new ResponseModel();
            r.Profile = new Profile();
            r.Profile.Id = id;
            return View(r);
            
        }


        public ActionResult ProfileInfo(int id)
        {
            Profile p = new Profile();
            ResponseModel responseModel = repository.FindAllFor(id);
            return View("ProfileInfo", responseModel);
        }

        //----------------------------------
        public ActionResult Edit(int id=1)
        {
            Profile p = new Profile();
            ResponseModel responseModel = repository.FindAllFor(id);
            return View("Edit", responseModel);
        }

        //-----------------------------------------
        public ActionResult SearchForm(int id = 1)
        {
            Profile p = new Profile();
            ResponseModel responseModel = repository.FindAllFor(id);
            return View("SearchForm", responseModel);
        }

        //---------------------------------
        public ActionResult Search(int id = 1)
        {
           
            ProfileAndSearchView p = ((ProfileRepository)repository).GetProfileAndSearchInfoBy(id);
            return View("Search", p);
        }

        //---------------------------------
        public ActionResult Save(Profile Profile, int id)
        {

            Profile.Id = id;
            repository.Save(Profile);
            return View("Index1", Profile );
        }


        //------------------------------------------------
        public ActionResult List(Profile Profile, int id)
        {
            Profile.Id = id;
            IEnumerable<Profile> pp = ((ProfileRepository)repository).FindByQuery(Profile);

            return View("List", pp);
        }
        //----------------------------------------------------

        

       
        public ActionResult Details(int idForDisplay, int idForMenu)
        {

            ProfileForDisplay pp = new ProfileForDisplay();// repository.FindBy(idForMenu, idForDisplay);


            pp.Profile = repository.FindBy(idForDisplay, true);
    

            return View("Details", pp);
        }



       //---------------------------------------------------------


        public ActionResult SearchSearch(int id = 1, bool useNumerology = false, bool useZodiac = false)
        {
            ResponseModel responseModel = repository.FindByQuery(id, useNumerology, useZodiac);

           return View("List", responseModel);           
        
        }     

//@using (Ajax.BeginForm("AjaxSearch", "Profile", new AjaxOptions(){ OnSuccess= "OnSuccess"}))

        //--------------------------------------------------------------------------------------
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [HttpPost]
        public JsonResult AjaxSearch(int id, bool useNumerology = false, bool useZodiac = false)
        {
           
            ResponseModel responseModel = repository.FindByQuery(id,  useNumerology, useZodiac);

            return Json(new
            {
                total1 = "0",
                total2 = responseModel.AllProfilesByNumerology.ToString(),
                total3 = "0",
                totals = responseModel.AllProfiles.ToString()
            },
                JsonRequestBehavior.AllowGet);
        }


        //-----------------------------------------------------
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [HttpPost]
        public JsonResult AjaxSearchResultView(int id, bool useNumerology = false, bool useZodiac = false)
        {

            ResponseModel responseModel = repository.FindByQuery(id, useNumerology, useZodiac);

            // ImageId = p.ProfileImages.Where( s => s.ImageOrderId == 1).FirstOrDefault().Id
            var result = responseModel.Profiles.Select( p => new {
                             //ImageId = p.ProfileImages.Where( s => s.ImageOrderId == 1).FirstOrDefault().Id,
                             DateBirth = p.DateBirth.ToString("MMMM dd, yyyy"),
                             ImageId = p.ProfileImages.OrderBy(s => s.ImageOrderId).Select(s => s.Id),
                             FirstName = p.FirstName,
                             GenderId = p.GenderId,
                             Zodiac = p.Zodiac, Id = p.Id});

            return Json( result, JsonRequestBehavior.AllowGet);
        }     

        
        
        //--------------------------------------------------------------------------------
        public JsonResult SearchTotals(ResponseModel responseModel)
        {
            return Json(new { 
                total1 = responseModel.AllProfilesByNumerology.ToString(),
                total2 = responseModel.AllProfiles.ToString() ,
                 total3 = responseModel.AllProfiles.ToString(),
                     totals = responseModel.AllProfiles.ToString() },
                JsonRequestBehavior.AllowGet);
        }

        //-------------------------------------------------------------------------



             public ActionResult SearchForPage(Profile Profile,  int Submit, 
                 int pageNumber=1, int itemsPerPage=4, int id = 0, bool useNumerology=false)
        {

            if (Submit == 3) return View("Index1");
        
            bool useZeroId = true;
            //bool useZeroId = false;

            Profile.Id = id;
            ResponseModel responseModel = repository.FindByQuery(id, useZeroId, useNumerology);

            IEnumerable<ProfileForDisplay> profilesForDisplay = null;
              //  repository.FindAllForPage(responseModel.Profiles, pageNumber, itemsPerPage);

            if (Submit == 2) return View("ProfileInfo", responseModel); // submit 1 to see view list

            else return View("ListWithDetails", profilesForDisplay); //submit 2 to see Counts

        }



        //---------------------------------------------------
        public ActionResult Help(int id = 0)
        {
            ResponseModel responseModel = new ResponseModel();
            responseModel.Profile = new Profile();
            responseModel.Profile.Id = id;

            return View("Help", responseModel); 
        }        
    }
}
