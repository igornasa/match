﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;




namespace Match.WebUI.Infrastructure.HTMLHelpers
{     // must to add <add namespace="ProjectMVC.WebUI.Infrastructure.HtmlHelpers.PriceHelper"/> 
    //Добавляем пространство имен вспомогательного метода HTML в файл  Views/Web.config

    public static class PriceHelper
    {
        public static string FormatMoney(this decimal price)
        {
            return String.Format("${0}", price);
        }
    }

}
